/**
 * @format
 */

import 'react-native-gesture-handler'
import { AppRegistry } from 'react-native'
import App from 'app/index.js'
import { BaseSetting } from '@config'
// import App from './App'
// import {name as appName} from './app.json';

AppRegistry.registerComponent(BaseSetting.name, () => App)
