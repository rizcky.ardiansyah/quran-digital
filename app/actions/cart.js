import * as actionTypes from './actionTypes'

const setCart = cart => {
	return {
		type: actionTypes.CART,
		cart,
	}
}

export const onSetCart = cart => dispatch => {
	dispatch(setCart(cart))
}
