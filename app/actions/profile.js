import * as actionTypes from './actionTypes'

const setProfile = prof => {
	return {
		type: actionTypes.PROFILE,
		prof,
	}
}

export const onSetProfile = prof => dispatch => {
	dispatch(setProfile(prof))
}

const setProfileDetail = profdetail => {
	return {
		type: actionTypes.GET_PROFILE,
		profdetail,
	}
}

export const onSetProfileDetail = profdetail => dispatch => {
	dispatch(setProfileDetail(profdetail))
}

const setToken = token => {
	return {
		type: actionTypes.TOKEN,
		token,
	}
}

export const onSetToken = token => dispatch => {
	console.log('onSetToken Set : ' + JSON.stringify(token))
	dispatch(setToken(token))
}

const setImg = img => {
	return {
		type: actionTypes.IMAGE,
		img,
	}
}

export const onSetImg = img => dispatch => {
	console.log('PRofile Set : ' + JSON.stringify(img))
	dispatch(setImg(img))
}

const setGambar = gambar => {
	return {
		type: actionTypes.GAMBAR,
		gambar,
	}
}

export const onSetGambar = gambar => dispatch => {
	console.log('onSetGambar Set : ' + JSON.stringify(gambar))
	dispatch(setGambar(gambar))
}

const setGita = gita => {
	return {
		type: actionTypes.GITA,
		gita,
	}
}

export const onSetGita = gita => dispatch => {
	console.log('onSetGita Set : ' + JSON.stringify(gita))
	dispatch(setGita(gita))
}

const setSign = sign => {
	return {
		type: actionTypes.SIGN,
		sign,
	}
}

export const onSetSign = sign => dispatch => {
	console.log('onSetSign Set : ' + JSON.stringify(sign))
	dispatch(setSign(sign))
}
