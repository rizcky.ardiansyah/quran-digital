const bangkitDariRukuData = [
  {
    id: '1',
    judul: 'bacaan bangkit dari ruku',
    doa: 'سَمِعَ اللَّهُ لِمَنْ حَمِدَهُ',
    lafal: 'Sami’allaahu liman hamidah',
    arti: 'Semoga Allah mendengar pujian orang yang memujiNya',
    keterangan: 'dibaca 1x',
    hadist: 'HR. Bukhari',
    notes: '',
  },
];

export {bangkitDariRukuData};
