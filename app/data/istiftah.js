const istiftahData = [
  {
    id: '1',
    judul: 'bacaan istiftah',
    doa:
      'سُبْحَانَكَ اللَّهُمَّ وَبِحَمْدِكَ وَتَبَارَكَ اسْمُكَ وَتَعَالَى جَدُّكَ وَلاَ إِلَهَ غَيْرُكَ',
    lafal:
      'SUBHAANAKALLOHUMMA WA BI HAMDIKA WA TABAAROKASMUKA WA TA’AALAAJADDUKA WA LAA ILAHA GHOIRUK',
    arti:
      'Maha suci Engkau ya Allah, aku memuji-Mu, Maha berkah Nama-Mu. Maha tinggi kekayaan dan kebesaran-Mu, tidak ada sesembahan yang berhak diibadahi dengan benar selain Engkau',
    keterangan: 'dibaca 1x',
    hadist:
      'HR. Muslim, no. 399; Abu Daud, no. 775; Tirmidzi, no. 242; Ibnu Majah, no. 804',
    notes: '',
  },
  {
    id: '2',
    judul: 'bacaan istiftah',
    doa:
      'اللَّهُمَّ بَاعِدْ بَيْنِي وَبَيْنَ خَطَايَايَ كَمَا بَاعَدْتَ بَيْنَ الْمَشْرِقِ وَالْمَغْرِبِ اللَّهُمَّ نَقِّنِي مِنْ خَطَايَايَ كَمَا يُنَقَّى الثَّوْبُ الْأَبْيَضُ مِنْ الدَّنَسِ اللَّهُمَّ اغْسِلْنِي مِنْ خَطَايَايَ بِالْمَاءِ وَالثَّلْجِ وَالْبَرَدِ',
    lafal:
      'ALLOHUMMA BAA’ID BAYNII WA BAYNA KHOTHOYAAYA KAMAA BAA’ADTA BAYNAL MASYRIQI WAL MAGHRIB. ALLOHUMMA NAQQINII MIN KHOTHOYAAYA KAMAA YUNAQQOTS TSAUBUL ABYADHU MINAD DANAS. ALLOHUMMAGH-SILNII MIN KHOTHOYAAYA BIL MAA-I WATS TSALJI WAL BAROD',
    arti:
      'Ya Allah, jauhkanlah antara aku dan kesalahan-kesalahanku, sebagaimana Engkau menjauhkan antara timur dan barat. Ya Allah, bersihkanlah aku dari kesalahan-kesalahanku sebagaimana baju putih dibersihkan dari kotoran. Ya Allah, cucilah aku dari kesalahan-kesalahanku dengan air, salju dan embun',
    keterangan: 'dibaca 1x',
    hadist:
      'HR. Bukhari,no. 744;Muslim,no. 598;An-Nasa’i,no. 896;teks haditsnyaadalah dari An-Nasa’i',
    notes:
      'Doa ini biasa dibaca Rasulullah Shallallahu’alaihi Wasallam dalam shalat fardhu. Doa ini adalah doa yang paling shahih diantara doa istiftah lainnya, sebagaimana dikatakan oleh Ibnu Hajar dalam Fathul Baari (2/183)',
  },
  {
    id: '3',
    judul: 'bacaan istiftah',
    doa:
      'اللَّهُمَّ رَبَّ جِبْرَائِيلَ وَمِيكَائِيلَ وَإِسْرَافِيلَ فَاطِرَ السَّمَوَاتِ وَالأَرْضِ عَالِمَ الْغَيْبِ وَالشَّهَادَةِ أَنْتَ تَحْكُمُ بَيْنَ عِبَادِكَ فِيمَا كَانُوا فِيهِ يَخْتَلِفُونَ اِهْدِنِى لِمَا اخْتُلِفَ فِيهِ مِنَ الْحَقِّ بِإِذْنِكَ إِنَّكَ تَهْدِى مَنْ تَشَاءُ إِلَى صِرَاطٍ مُسْتَقِيمٍ',
    lafal:
      'ALLOHUMMA ROBBA JIBROO-IILA WA MII-KA-IILA WA ISROOFIILA, FAATHIROS SAMAAWAATI WAL ARDHI ‘ALIIMAL GHOIBI WASY SYAHAADAH ANTA TAHKUMU BAYNA ‘IBAADIKA FIIMAA KAANUU FIIHI YAKHTALIFUUN, IHDINII LIMAKHTULIFA FIIHI MINAL HAQQI BI-IDZNIK, INNAKA TAHDI MAN TASYAA-U ILAA SHIROOTIM MUSTAQIIM',
    arti:
      'Ya Allah, Rabbnya Jibril, Mikail dan Israfil. Wahai Pencipta langit dan bumi. Wahai Rabb yang mengetahui yang ghaib dan nyata. Engkau yang menjatuhkan hukum untuk memutuskan apa yang mereka pertentangkan. Tunjukkanlah aku pada kebenaran apa yang dipertentangkan dengan seizin dari-Mu. Sesungguhnya Engkau menunjukkan pada jalan yang lurus bagi orang yang Engkau kehendaki',
    keterangan: 'dibaca 1x',
    hadist: 'HR. Muslim, no. 770',
    notes: 'Biasa dibaca oleh Nabi ﷺ ketika shalat malam',
  },
  {
    id: '4',
    judul: 'bacaan istiftah',
    doa:
      'اللَّهُ أَكْبَرُ كَبِيرًا اللَّهُ أَكْبَرُ كَبِيرًا اللَّهُ أَكْبَرُ كَبِيرًا وَالْحَمْدُ لِلَّهِ كَثِيرًا وَالْحَمْدُ لِلَّهِ كَثِيرًا وَالْحَمْدُ لِلَّهِ كَثِيرًا وَسُبْحَانَ اللَّهِ بُكْرَةً وَأَصِيلاً وَسُبْحَانَ اللَّهِ بُكْرَةً وَأَصِيلاً وَسُبْحَانَ اللَّهِ بُكْرَةً وَأَصِيلاً أَعُوذُ بِاللَّهِ مِنَ الشَّيْطَانِ مِنْ نَفْخِهِ وَنَفْثِهِ وَهَمْزِهِ',
    lafal:
      'ALLOHU AKBAR KABIIRO, ALLOHU AKBAR KABIIRO, ALLOHU AKBAR KABIIRO, WALHAMDULILLAHI KATSIIRO, WALHAMDULILLAHI KATSIIRO, WALHAMDULILLAHI KATSIIRO, WA SUBHANALLAHI BUKROTAW WASHIILAA, WA SUBHANALLAHI BUKROTAW WASHIILAA, WA SUBHANALLAHI BUKROTAW WASHIILA A’UDZU BILLAHI MINASY SYAITHOONI MIN NAFKHIHI, WA NAFTSHIHI, WA HAMZIH',
    arti:
      'Allah Maha Besar, Allah Maha Besar, Allah Maha Besar. Segala puji bagi Allah dengan pujian yang banyak, segala puji bagi Allah dengan pujian yang banyak, segala puji bagi Allah dengan pujian yang banyak. Maha Suci Allah di waktu pagi dan sore. Maha Suci Allah di waktu pagi dan sore. Maha Suci Allah di waktu pagi dan sore. Aku berlindung kepada Allah dari tiupan, bisikan, dan godaan setan',
    keterangan: 'dibaca 1x',
    hadist:
      'HR. Abu Daud, no. 764; Ibnu Majah, no. 807; Ahmad, 4:80,85. Syaikh Syu’aib Al-Arnauth dan ‘Abdul Qadir Al-Arnauth dalam tahqiq Zaad Al-Ma’ad, 1:197 mengatakan bahwa hadits ini dishahihkan oleh Ibnu Hibban, Al-Hakim dan disetujui oleh Imam Adz-Dzahabi',
    notes: '',
  },
  {
    id: '5',
    judul: 'bacaan istiftah',
    doa:
      'وَجَّهْتُ وَجْهِيَ لِلَّذِي فَطَرَ السَّمَاوَاتِ وَالْأَرْضَ حَنِيفًا، وَمَا أَنَا مِنَ الْمُشْرِكِينَ، إِنَّ صَلَاتِي، وَنُسُكِي، وَمَحْيَايَ، وَمَمَاتِي لِلَّهِ رَبِّ الْعَالَمِينَ، لَا شَرِيكَ لَهُ، وَبِذَلِكَ أُمِرْتُ وَأَنَا مِنَ الْمُسْلِمِينَ، اللهُمَّ أَنْتَ الْمَلِكُ لَا إِلَهَ إِلَّا أَنْتَ أَنْتَ رَبِّي، وَأَنَا عَبْدُكَ، ظَلَمْتُ نَفْسِي، وَاعْتَرَفْتُ بِذَنْبِي، فَاغْفِرْ لِي ذُنُوبِي جَمِيعًا، إِنَّهُ لَا يَغْفِرُ الذُّنُوبَ إِلَّا أَنْتَ، وَاهْدِنِي لِأَحْسَنِ الْأَخْلَاقِ لَا يَهْدِي لِأَحْسَنِهَا إِلَّا أَنْتَ، وَاصْرِفْ عَنِّي سَيِّئَهَا لَا يَصْرِفُ عَنِّي سَيِّئَهَا إِلَّا أَنْتَ، لَبَّيْكَ وَسَعْدَيْكَ وَالْخَيْرُ كُلُّهُ فِي يَدَيْكَ، وَالشَّرُّ لَيْسَ إِلَيْكَ، أَنَا بِكَ وَإِلَيْكَ، تَبَارَكْتَ وَتَعَالَيْتَ، أَسْتَغْفِرُكَ وَأَتُوبُ إِلَيْك',
    lafal:
      'wajjahtu wajhiya lilladzii fathorossamaawaati wal ardhi haniifaa, wa maa anaa minal musyrikiin, inna sholaatii, wa nusukii, wa mahyaaya, wa mamaatii lillaahi robbil aalamiin, laa syariikalahu, wa bidzalika umirtu wa anaa minal muslimiin, Allaahumma antal maliku laa ilaha illaa anta anta robbii, wa anaa abduka, dzolamtu nafsii, wa taraftu bidzanbii, faghfirlii dzunuubii jamiiaan, innahu laa yaghfirudzdzunuuba illaa anta, wahdinii liahsanil akhlaaqi laa yahdii liahsanihaa illa anta, wahsrif anni sayyiahaa laa yashrifu anni sayyiahaa illa anta, labbaika wasadaika wal khoiru kulluhu fii yadaika, wash sharru laisa ilaika, anaa bika, wa ilaika, tabaa rakta wa taaalaita, astaghfiruka wa atuubu ilaika',
    arti:
      'Aku hadapkan wajahku kepada Dzat yang Maha Pencipta langit dan bumi sebagai muslim yang ikhlas dan aku bukan termasuk orang yang musyrik. Sesungguhnya shalatku, sembelihanku, hidupku dan matiku, hanya semata-mata untuk Allah Rabb semesta alam. Tidak ada sekutu bagiNya. Oleh karena itu aku patuh kepada perintahNya, dan aku termasuk orang yang aku berserah diri. Ya Allah, Engkaulah Maha Penguasa. Tidak ada Ilah yang berhak disembah selain Engkau. Mahasuci Engkau dan Maha Terpuji. Engkaulah Tuhanku dan aku adalah hambaMu. Aku telah menzhalimi diriku sendiri dan akui dosa-dosaku. Karena itu ampunilah dosa-dosaku semuanya. Sesungguhnya tidak ada yang bisa mengampuni segala dosa melainkan Engkau. Tunjukilah aku akhlak yang paling terbaik. Tidak ada yang dapat menunjukkannya melainkan hanya Engkau. Jauhkanlah akhlak yang buruk dariku, karena sesungguhnya tidak ada yang sanggup menjauhkannya melainkan hanya Engkau. Aka aku patuhi segala perintah-Mu, dan akan aku tolong agama-Mu. Segala kebaikan berada di tangan-Mu. Sedangkan keburukan tidak datang dari Mu. Orang yang tidak tersesat hanyalah orang yang Engkau beri petunjuk. Aku berpegang teguh dengan-Mu dan kepada-Mu. Tidak ada keberhasilan dan jalan keluar kecuali dari Mu. Maha Suci Engkau dan Maha Tinggi. Kumohon ampunan dariMu dan aku bertobat kepadaMu',
    hadist: 'HR. Muslim 2/185 - 186',
    notest:
      'Doa ini biasa dibaca Rasulullah Shallallahu’alaihi Wasallam dalam shalat fardhu dan shalat sunnah',
  },
  {
    id: '6',
    judul: 'bacaan istiftah',
    doa:
      'اللَّهِ أَكْبَرُ وَجَّهْتُ وَجْهِيَ لِلَّذِي فَطَرَ السَّمَوَاتِ وَالْأَرْضَ حَنِيفًا مُسْلِمًا وَمَا أَنَا مِنَ الْمُشْرِكِينَ، إِنَّ صَلَاتِي وَنُسُكِي وَمَحْيَايَ وَمَمَاتِي لِلَّهِ رَبِّ الْعَالَمِينَ لَا شَرِيكَ لَهُ وَبِذَلِكَ أُمِرْتُ وَأَنَا أَوَّلُ الْمُسْلِمِينَ، اللَّهُمَّ أَنْتَ الْمَلِكُ لَا إِلَهَ إِلَّا أَنْتَ سُبْحَانَكَ وَبِحَمْدِك',
    lafal:
      'wajjahtu wajhiya lilladzii fathorossamaawaati wal ardhi haniifaa, wa maa anaa minal musyrikiin, inna sholaatii, wa nusukii, wa mahyaaya, wa mamaatii lillaahi robbil aalamiin, laa syariikalahu, wa bidzalika umirtu wa anaa minal muslimiin, Allaahumma antal maliku laa ilaha illaa anta anta robbii, wa anaa abduka, dzolamtu nafsii, wa taraftu bidzanbii, faghfirlii dzunuubii jamiiaan, innahu laa yaghfirudzdzunuuba illaa anta, wahdinii liahsanil akhlaaqi laa yahdii liahsanihaa illa anta, wahsrif anni sayyiahaa laa yashrifu anni sayyiahaa illa anta, labbaika wasadaika wal khoiru kulluhu fii yadaika, wash sharru laisa ilaika, anaa bika, wa ilaika, tabaa rakta wa taaalaita, astaghfiruka wa atuubu ilaika',
    arti:
      'Aku hadapkan wajahku kepada Dzat yang Maha Pencipta langit dan bumi sebagai muslim yang ikhlas dan aku bukan termasuk orang yang musyrik. Sesungguhnya shalatku, sembelihanku, hidupku dan matiku, hanya semata-mata untuk Allah Rabb semesta alam. Tidak ada sekutu bagi-Nya. Oleh karena itu aku patuh kepada perintahNya, dan aku termasuk orang yang aku berserah diri. Ya Allah, Engkaulah Maha Penguasa. Tidak ada Ilah yang berhak disembah selain Engkau. Mahasuci Engkau dan Maha Terpuji',
    hadist: 'HR. An Nasa-i 1/143',
    notest: '',
  },
  {
    id: '7',
    judul: 'bacaan istiftah',
    doa:
      'إِنَّ صَلَاتِي وَنُسُكِي وَمَحْيَايَ وَمَمَاتِي لِلَّهِ رَبِّ الْعَالَمِينَ لَا شَرِيكَ لَهُ، وَبِذَلِكَ أُمِرْتُ وَأَنَا مِنَ الْمُسْلِمِينَ. اللَّهُمَّ اهْدِنِي لِأَحْسَنِ الْأَعْمَالِ وَأَحْسَنِ الْأَخْلَاقِ لَا يَهْدِي لِأَحْسَنِهَا إِلَّا أَنْتَ، وَقِنِي سَيِّئَ الْأَعْمَالِ وَسَيِّئَ الْأَخْلَاقِ لَا يَقِي سَيِّئَهَا إِلَّا أَنْت',
    lafal:
      'wajjahtu wajhiya lilladzii fathorossamaawaati wal ardhi haniifaa, wa maa anaa minal musyrikiin, inna sholaatii, wa nusukii, wa mahyaaya, wa mamaatii lillaahi robbil aalamiin, laa syariikalahu, wa bidzalika umirtu wa anaa minal muslimiin, Allaahumma antal maliku laa ilaha illaa anta anta robbii, wa anaa abduka, dzolamtu nafsii, wa taraftu bidzanbii, faghfirlii dzunuubii jamiiaan, innahu laa yaghfirudzdzunuuba illaa anta, wahdinii liahsanil akhlaaqi laa yahdii liahsanihaa illa anta, wahsrif anni sayyiahaa laa yashrifu anni sayyiahaa illa anta, labbaika wasadaika wal khoiru kulluhu fii yadaika, wash sharru laisa ilaika, anaa bika, wa ilaika, tabaa rakta wa taaalaita, astaghfiruka wa atuubu ilaika',
    arti:
      'Sesungguhnya shalatku, sembelihanku, hidupku dan matiku, hanya semata-mata untuk Allah Rabb semesta alam. Tidak ada sekutu bagi-Nya. Oleh karena itu aku patuh kepada perintahNya, dan aku termasuk orang yang aku berserah diri. Ya Allah, tunjukilah aku amal dan akhlak yang terbaik. Tidak ada yang dapat menujukkanku kepadanya kecuali Engkau. Jauhkanlah aku dari amal dan akhlak yang buruk. Tidak ada yang dapat menjauhkanku darinya kecuali Engkau',
    hadist: 'HR. An Nasa-i 1/141, Ad Daruquthni 112',
    notest: '',
  },
  {
    id: '8',
    judul: 'bacaan istiftah',
    doa: 'سُبْحَانَكَ اللَّهُمَّ وَبِحَمْدِكَ وَتَبَارَكَ اسْمُكَ، وَتَعَالَى جَدُّكَ، وَلَا إِلَهَ غَيْرَكَ
      3x  لَا إِلَهَ إِلَّا اللَّهُ
      3x  اللَّهُ أَكْبَرُ كَبِيرً',
    lafal: 'subhaanaka allaahumma wa bihamdika wa tabaarakasmuka, wa taalaa jadduka, wa laa ilaha ghoriuka, laailaha illalahhu (3x), allahahu akbar kabiiraa',
    arti: 'Maha suci Engkau, ya Allah. Ku sucikan nama-Mu dengan memuji-Mu. Nama-Mu penuh berkah. Maha tinggi Engkau. Tidak ilah yang berhak disembah selain Engkau, Tiada Tuhan yang berhak disembah selain Allah (3x), Allah Maha Besar (3x)',
    hadist: 'HR.Abu Daud 1/124, dihasankan oleh Al Albani dalam Sifatu Shalatin Nabi 1/252',
    notest: '',
  },
  {
    id: '9',
    judul: 'bacaan istiftah',
    doa: 'اللهُ أَكْبَرُ كَبِيرًا، وَالْحَمْدُ لِلَّهِ كَثِيرًا، وَسُبْحَانَ اللهِ بُكْرَةً وَأَصِيلًا',
    lafal:'allaahu akbaru kabiiraa, walhamdulillaahi katsiiraa, wa subhaanallahi bukratawwasashiilaa',
    arti: 'Maha suci Engkau, ya Allah. Ku sucikan nama-Mu dengan memuji-Mu. Nama-Mu penuh berkah. Maha tinggi Engkau. Tidak ilah yang berhak disembah selain Engkau, Tiada Tuhan yang berhak disembah selain Allah (3x), Allah Maha Besar (3x)',
    hadist: 'HR.Abu Daud 1/124, dihasankan oleh Al Albani dalam Sifatu Shalatin Nabi 1/252',
    notest: '',
  },
  {
    id: '10',
    judul: 'bacaan istiftah',
    doa: 'الْحَمْدُ لِلَّهِ حَمْدًا كَثِيرًا طَيِّبًا مُبَارَكًا فِيهِ',
    lafal:'alhamdullillahi hamdaan katsiraa, thoyyiban mubaarakaan fiihi',
    arti: 'Segala puji bagi Allah dengan pujian yang banyak, pujian yang terbaik dan pujian yang penuh keberkahan di dalamnya',
    hadist: 'HR. Muslim 2/99',
    notest: '',
  },
  {
    id: '11',
    judul: 'bacaan istiftah',
    doa: 'اللهُمَّ رَبَّ جَبْرَائِيلَ، وَمِيكَائِيلَ، وَإِسْرَافِيلَ، فَاطِرَ السَّمَاوَاتِ وَالْأَرْضِ، عَالِمَ الْغَيْبِ وَالشَّهَادَةِ، أَنْتَ تَحْكُمُ بَيْنَ عِبَادِكَ فِيمَا كَانُوا فِيهِ يَخْتَلِفُونَ، اهْدِنِي لِمَا اخْتُلِفَ فِيهِ مِنَ الْحَقِّ بِإِذْنِكَ، إِنَّكَ تَهْدِي مَنْ تَشَاءُ إِلَى صِرَاطٍ مُسْتَقِيمٍ',
    lafal:'Ya Allah, Rabb-nya malaikat Jibril, Mikail, dan Israfil. Pencipta langit dan bumi. Yang mengetahui hal ghaib dan juga nyata. Engkaulah hakim di antara hamba-hamba-Mu dalam hal-hal yang mereka perselisihkan. Tunjukkanlah aku kebenaran dalam apa yang diperselisihkan, dengan izin-Mu. Sesungguhnya Engkau memberi petunjuk menuju jalan yang lurus, kepada siapa saja yang Engkau kehendaki',
    arti: 'Segala puji bagi Allah dengan pujian yang banyak, pujian yang terbaik dan pujian yang penuh keberkahan di dalamnya',
    hadist: 'HR. Muslim 2/185',
    notest: '',
  },
];

export {istiftahData};
