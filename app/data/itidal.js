const itidalData = [
  {
    id: '1',
    judul: 'bacaan itidal',
    doa: 'رَبَّنَا لَكَ الحَمْدُ',
    lafal: 'Robbanaa lakal hamdu',
    arti: 'Ya Tuhan kami, bagi-Mu segala puji',
    keterangan: 'dibaca 1x',
    hadist: 'HR. Bukhari no. 722',
    notes: '',
  },
  {
    id: '2',
    judul: 'bacaan itidal',
    doa: 'اَللَّهُمَّ رَبَّنَا لَكَ الحَمْدُ',
    lafal: 'Allaahumma robbanaa lakal hamdu',
    arti: 'Ya Allah, Ya Tuhan kami, bagi-Mu segala puji',
    keterangan: 'dibaca 1x',
    hadist: 'HR. Bukhori No: 796 Muslim no. 902 dari Abu Musa al-Asy’ari',
    notes: '',
  },
  {
    id: '3',
    judul: 'bacaan itidal',
    doa: 'رَبَّنَا وَلَكَ الحَمْدُ',
    lafal: 'Robbanaa walakal hamdu',
    arti: 'Ya Tuhan kami, dan bagi-Mu segala puji',
    keterangan: 'dibaca 1x',
    hadist: 'HR. al-Bukhari no.732 no.689 dan Muslim no.866 dari Abu Hurairah',
    notes: '',
  },
  {
    id: '4',
    judul: 'bacaan itidal',
    doa: 'اَللَّهُمَّ رَبَّنَا وَلَكَ الحَمْدُ',
    lafal: 'Allaahumma robbanaa walakal hamdu',
    arti: 'Ya Allah, Ya Tuhan kami, dan bagi-Mu segala puji',
    keterangan: 'dibaca 1x',
    hadist: 'HR. al-Bukhari no.795',
    notes: '',
  },
  {
    id: '5',
    judul: 'bacaan itidal',
    doa: 'رَبَّنَا وَلَكَ الحَمْدُ حَمْدًا كَثِيرًا طَيِّبًا مُبَارَكًا فِيهِ',
    lafal: 'Robbanaa walakal hamdu hamdan katsiiron toyyiban mubaarokan fiih',
    arti:
      'Ya Tuhan kami, dan bagi-Mu segala puji, dengan pujian yang banyak, yang baik, yang diberkahi di dalamnya.',
    keterangan: 'dibaca 1x',
    hadist: 'HR. Bukhori No: 799 dari hadits Rifa’ah ibnu Rafi',
    notes:
      'Pernah seseorang yang shalat di belakang Rasulullah membaca: (doa di atas) saat bangkit dari ruku’ setelah ucapan “Sami’allaahu liman hamidah”. Selesai dari shalat, Rasulullah bertanya, “Siapa yang mengucapkannya tadi?” Orang itu berkata, “Saya, wahai Rasulullah.” Rasulullah bersabda, “Sungguh aku melihat lebih dari 30 malaikat berlomba-lomba, siapa di antara mereka yang paling dahulu mencatatnya',
  },
  {
    id: '6',
    judul: 'bacaan itidal',
    doa:
      'اَللَّهُمَّ رَبَّنَا لَكَ الْحَمْدُ، مِلْءُ السَّمَاوَاتِ، وَمِلْءُ الْأَرْضِ وَمِلْءُ مَا شِئْتَ مِنْ شَيْءٍ بَعْدُ',
    lafal:
      'Allaahumma robbanaa lakal hamdu, mil-us samaawaati, wa mil-ul ardli, wa mil-u maa syi’ta min syai-in ba’du',
    arti:
      'Ya Allah, ya Tuhan kami, bagi-Mu segala puji, sepenuh langit, sepenuh bumi, sepenuh apapun yang Engkau kehendaki setelah itu',
    keterangan: 'dibaca 1x',
    hadist:
      'HR. Muslim No: 476(HR. Muslim no. 1067 dari hadits Abdullah ibnu Abi Aufa',
    notes: '',
  },
  {
    id: '7',
    judul: 'bacaan itidal',
    doa:
      'اَللَّهُمَّ رَبَّنَا لَكَ الْحَمْدُ مِلْءَ السَّمَاوَاتِ، وَمِلْءَ الْأَرْضِ، وَمِلْءَ مَا بَيْنَهُمَا، وَمِلْءَ مَا شِئْتَ مِنْ شَيْءٍ بَعْدُ',
    lafal:
      'Allaahumma robbanaa lakal hamdu, mil-us samaawaati, wa mil-ul ardli, wa mil-u maa bainahumaa, wa mil-u maa syi’ta min syai-in ba’du',
    arti:
      'Ya Allah, ya Tuhan kami, bagi-Mu segala puji, sepenuh langit, sepenuh bumi, sepenuh apa yang ada di antara keduanya, sepenuh apapun yang Engkau kehendaki setelah itu',
    keterangan: 'dibaca 1x',
    hadist: 'HR. Muslim No: 771',
    notes: '',
  },
  {
    id: '8',
    judul: 'bacaan itidal',
    doa:
      'للَّهُمَّ رَبَّنَا لَكَ الْحَمْدُ، مِلْءُ السَّمَاوَاتِ وَمِلْءُ الْأَرْضِ، وَمَا بَيْنَهُمَا، وَمِلْءُ مَا شِئْتَ مِنْ شَيْءٍ بَعْدُ، أَهْلَ الثَّنَاءِ وَالْمَجْدِ، لَا مَانِعَ لِمَا أَعْطَيْتَ، وَلَا مُعْطِيَ لِمَا مَنَعْتَ، وَلَا يَنْفَعُ ذَا الْجَدِّ مِنْكَ الْجَدُّ',
    lafal:
      'Allaahumma robbanaa lakal hamdu, mil-us samaawaati, wa mil-ul ardli, wa maa bainahumaa, wa mil-u maa syi’ta min syai-in ba’du, ahlats tsanaa-i wal majdi, laa maani’a limaa a’toita, wa laa mu’tiya limaa mana’ta, wa laa yanfa’u dzal jaddi minkal jaddu',
    arti:
      'Ya Allah, ya Tuhan kami, bagi-Mu segala puji, sepenuh langit, sepenuh bumi, dan apa yang ada di antara keduanya, sepenuh apapun yang Engkau kehendaki setelah itu. Yang berhak disanjung dan dimuliakan. Tidak ada yang dapat menghalangi apa yang akan Engkau berikan, tidak ada yang dapat memberikan apa yang Engkau tahan, dan tidak bermanfaat suatu kekayaan, terhadap orang yang memiliki kekayaan, dari keputusan-Mu',
    keterangan: 'dibaca 1x',
    hadist: 'HR. Muslim No 478 & no 1072 dari Ibnu Abbas',
    notes: '',
  },
  {
    id: '9',
    judul: 'bacaan itidal',
    doa:
      'رَبَّنَا لَكَ الْحَمْدُ مِلْءُ السَّمَاوَاتِ وَالْأَرْضِ، وَمِلْءُ مَا شِئْتَ مِنْ شَيْءٍ بَعْدُ، أَهْلَ الثَّنَاءِ وَالْمَجْدِ، أَحَقُّ مَا قَالَ الْعَبْدُ، وَكُلُّنَا لَكَ عَبْدٌ: اللهُمَّ لَا مَانِعَ لِمَا أَعْطَيْتَ، وَلَا مُعْطِيَ لِمَا مَنَعْتَ، وَلَا يَنْفَعُ ذَا الْجَدِّ مِنْكَ الْجَدُّ',
    lafal:
      'robbanaa lakal hamdu, mil-us samaawaati, wal ardli, wa mil-u maa syi’ta min syai-in ba’du, ahlats tsanaa-i wal majdi, ahaqqu maa qoolal ‘abdu, wa kullunaa laka ‘abdun, alloohumma laa maani’a limaa a’toita, wa laa mu’tiya limaa mana’ta, wa laa yanfa’u dzal jaddi minkal jaddu',
    arti:
      'Ya Tuhan kami, bagi-Mu segala puji, sepenuh langit dan bumi, dan sepenuh apapun yang Engkau kehendaki setelah itu. Yang berhak disanjung dan dimuliakan. Yang paling pantas dikatakan oleh seorang hamba, dan kami semua adalah hamba-Mu: Ya Allah, tidak ada yang dapat menghalangi apa yang akan Engkau berikan, tidak ada yang dapat memberikan apa yang Engkau tahan, dan tidak bermanfaat suatu kekayaan, terhadap orang yang memiliki kekayaan, dari keputusan-Mu',
    keterangan: 'dibaca 1x',
    hadist: 'HR. Muslim No.477 & no.1071 dari AbuSa’id al-Khudri',
    notes: '',
  },
  {
    id: '10',
    judul: 'bacaan itidal',
    doa: 'لِرَبِّيَ الْحَمْدُ، لِرَبِّيَ الْحَمْدُ',
    lafal: 'Lirobbiyal hamdu-lirobbiyal hamdu',
    arti: 'Hanya untuk Rabb-ku segala puji, hanya untuk Rabb-ku segala puji',
    keterangan: 'dibaca 1x',
    hadist:
      'HR. Nasai No: 1069 sahih. HR. Abu Dawud no. 874 dan yang lainnya dari hadits Hudzaifah, dinyatakan sahih dalam Shahih Sunan Abi Dawud dan al-Irwa’ no. 335',
    notes:
      'Beliau terus mengulang-ulangi ucapan ini, hingga lama berdirinya saat itu sama dengan lama ruku’nya, padahal lama ruku’ beliau mendekati lamanya beliau berdiri pada rakaat yang pertama, yang beliau membaca surat al-Baqarah',
  },
  {
    id: '11',
    judul: 'bacaan itidal',
    doa:
      'رَبَّنَا وَلَكَ الْحَمْدُ، حَمْدًا كَثِيْرًا طَيِّبًا مُبَارَكًا فِيْهِ، مُبَارَكًا عَلَيْهِ، كَمَا يُحِبُّ رَبُّنَا وَيَرْضَى',
    lafal:
      'Robbanaa wa lakal hamdu, hamdan katsiron thoyyiban mubaarokan fiihi, mubaarokan ‘alaihi kamaa yuhibbu robbunaa wa yardhoo',
    arti:
      'Wahai Rabb kami, bagi-Mu segala puji. Pujian yang banyak, yang baik, yang diberkahi di dalamnya, yang diberkahi di atasnya, sebagaimana Rabb kami senang dan ridha',
    keterangan: 'dibaca 1x',
    hadist: 'HR. Bukhari, Muslim dan yang lainnya',
    notes:
      'Ada sahabat yang membaca ini ketika i’tidal. Selesai shalat, Nabi shallallahu ‘alaihi wa sallam bertanya, “Siapa yang tadi membaca doa i’tidal tersebut?” Salah seorang sahabat mengaku. Nabi shallallahu ‘alaihi wa sallam bersabda, “Saya melihat ada 30 lebih malaikat yang berebut mengambil bacaan ini. Siapa di antara mereka yang paling cepat mencatatnya.”',
  },
  {
    id: '12',
    judul: 'bacaan itidal',
    doa:
      'رَبَّنَا لَكَ الْحَمْدُ مِلْءَ السَّمَاوَاتِ وَمِلْءَ اْلأَرْضِ وَمَا بَيْنَهُمَا، وَمِلْءَ مَا شِئْتَ مِنْ شَيْءٍ بَعْدُ، أَهْلَ الثَّنَاءِ وَالْمَجْدِ، أَحَقُّ مَا قَالَ الْعَبْدُ، وَكُلُّنَا لَكَ عَبْدٌ، اَللَّهُمَّ لَا مَانِعَ لِمَا أَعْطَيْتَ، وَلَا مُعْطِيَ لِمَا مَنَعْتَ، وَلَا يَنْفَعُ ذَا الْجَدِّ مِنْكَ الْجَدُّ',
    lafal:
      'Robbanaa lakal hamdu Mil-as-samaawaati wa mil-al ardhi wa maa bainahumaa, wa mil-a maa syi’ta min syai-in ba’du, ahlats-tsanaa-i wal majdi, ahaqqu maa qoolal ‘abdu, wa kullunaa laka ‘abdun, allaahumma laa maani’a limaa a’thoita, wa laa mu’thiya limaa mana’ta, wa laa yanfa’u dzal jaddi minkal jadd',
    arti:
      'Sepenuh langit dan sepenuh bumi, dan sepenuh apa yang ada di antara keduanya, dan sepenuh apa yang Engkau inginkan dari sesuatu setelahnya. Engkau adalah Dzat yang berhak mendapat pujian dan kemuliaan. (Ucapan ini) yang paling pantas diucapkan seorang hamba, dan semua kami adalah hamba-Mu semata. Ya Allah, tidak ada yang bisa menahan apa yang Engkau berikan, dan tidak ada yang bisa memberikan apa yang Engkau tahan. Tidak bermanfaat kekayaan bagi orang yang memilikinya (kecuali iman dan amal shalihnya), hanya dariMu kekayaan itu',
    keterangan: 'dibaca 1x',
    hadist: 'HR. Muslim 1/346',
    notes: '',
  },
];

export {itidalData};
