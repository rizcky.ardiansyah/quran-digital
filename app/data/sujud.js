const sujudData = [
  {
    id: '1',
    judul: 'bacaan sujud',
    doa: 'سُبْحَانَ رَبِّىَ الأَعْلَى',
    lafal: 'SUBHANAA ROBBIYAL A’LAA',
    arti: 'Mahasuci Rabbku Yang MahaTinggi',
    keterangan: 'dibaca 3x',
    hadist: 'HR. Muslim, no. 772 dan Abu Daud, no. 871',
  },
  {
    id: '2',
    judul: 'bacaan sujud',
    doa: 'سُبْحَانَ رَبِّىَ الأَعْلَى وَبِحَمْدِهِ',
    lafal: 'SUBHANAA ROBBIYAL A’LAA WA BI HAMDIH',
    arti: 'Mahasuci Rabbku Yang MahaTinggi dan pujian untuk-Nya',
    keterangan: 'dibaca 3x',
    hadist: 'HR. Abu Daud, no. 870, sahih',
  },
  {
    id: '3',
    judul: 'bacaan sujud',
    doa:
      'اللَّهُمَّ لَكَ سَجَدْتُ، وَبِكَ آمَنْتُ، وَلَكَ أَسْلَمْتُ، سَجَدَ وَجْهِي لِلَّذِي خَلَقَهُ وَصَوَّرَهُ، وَشَقَّ سَمْعَهُ وَبَصَرَهُ، تَبَارَكَ اللهُ أَحْسَنُ الخَالِقِينَ',
    lafal:
      'ALLAHUMMA LAKA SAJADTU, WA BIKA AAMANTU WA LAKA ASLAMTU, SAJADA WAJHI LILLADZI KHALAQAHU, WA SHAWWARAHU, WA SYAQQA SAM’AHU, WA BASHARAHU. TABARAKALLAHU AHSANUL KHOOLIQIIN',
    arti:
      'Ya Allah, kepada-Mu lah aku bersujud, karena-Mu juga aku beriman, kepada-Mu juga aku berserah diri. Wajahku bersujud kepada Penciptanya, yang Membentuknya, yang Membentuk pendengaran dan penglihatannya. Mahasuci Allah Sebaik-baik Pencipta',
    keterangan: 'dibaca 3x',
    hadist: 'HR. Muslim, no. 771',
  },
  {
    id: '4',
    judul: 'bacaan sujud',
    doa:
      'اللَّهُمَّ اغْفِرْ لِي ذَنْبِي كُلَّهُ : دِقَّهُ وَجِلَّهُ ، وَأَوَّلَهُ وَآخِرَهُ ، وَعَلاَنِيَتَهُ وَسِرَّهُ',
    lafal:
      'ALLOHUMMAGH-FIR LII DZANBII KULLAHU, DIQQOHU WA JILLAHU, WA AWWALAHU WA AAKHIROHU, WA ‘ALAANIYATAHU WA SIRROHU',
    arti:
      'Ya Allah ampunilah seluruh dosaku, yang kecilnya dan besarnya, yang pertamanya dan terakhirnya, yang terang-terangannya dan rahasianya',
    keterangan: 'dibaca 3x',
    hadist: 'HR. Muslim, no. 483',
  },
  {
    id: '5',
    judul: 'bacaan sujud',
    doa:
      'سُبْحَانَكَ اللَّهُمَّ رَبَّنَا وَبِحَمْدِكَ ، اللَّهُمَّ اغْفِرْ لِى',
    lafal: 'SUBHANAKALLAHUMMA ROBBANAA WA BIHAMDIKA, ALLAHUMMAGHFIR-LII',
    arti: 'Mahasuci Engkau Ya Allah, Rabb kami, pujian untuk-Mu, ampunilah aku',
    keterangan: 'dibaca 3x',
    hadist: 'HR. Bukhari, no. 817 dan Muslim, no. 484',
  },
  {
    id: '6',
    judul: 'bacaan sujud',
    doa: 'سُبُّوحٌ قُدُّوسٌ رَبُّ الْمَلاَئِكَةِ وَالرُّوحِ',
    lafal: 'SUBBUHUN QUDDUUS, ROBBUL MALAA-IKATI WAR RUUH',
    arti: 'Mahasuci, Maha Qudus, Rabbnya para malaikat dan ruh -yaitu Jibril-',
    keterangan: 'dibaca 3x',
    hadist: 'HR. Muslim, no. 487',
  },
  {
    id: '7',
    judul: 'bacaan sujud',
    doa:
      'سُبْحَانَ ذِي الجَبَرُوْتِ وَالملَكُوْتِ وَالكِبْرِيَاء ِوَالعَظَمَةِ',
    lafal: 'SUBHAANA DZIL JABARUUTI WAL MALAKUUTI WAL KIBRIYAA’ WAL ‘AZHOMAH',
    arti:
      'Mahasuci Allah Yang mempunyai keperkasaan dan kerajaan (penuh) serta kesombongan dan keagungan',
    keterangan: 'dibaca 3x',
    hadist: 'HR. Muslim, no. 487',
  },
];

export {sujudData};
