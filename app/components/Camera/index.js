import React, { useMemo, useState } from 'react';
import { StyleSheet, View, TouchableOpacity, TouchableWithoutFeedback, Dimensions, Platform } from 'react-native';
import { Image, Header, SafeAreaView, Icon, Text, Button, ProfileDetail, ProfilePerformance, ListThumbCircle } from '@components'
import { RNCamera } from 'react-native-camera';
import { BaseStyle, useTheme, Api,BaseColor } from '@config'
// eslint-disable-next-line
import Slider from '@react-native-community/slider';
import Modal from 'react-native-modal';
import { useCamera } from 'react-native-camera-hooks';
import {useTranslation} from 'react-i18next';
import styles from './styles';

const landmarkSize = 2;

const recordOptions = {
  mute: false,
  maxDuration: 5,
  quality: RNCamera.Constants.VideoQuality['288p'],
};

const initialState = {
    flash: 'off',
    zoom: 0,
    autoFocus: 'on',
    autoFocusPoint: {
      normalized: { x: 0.5, y: 0.5 }, // normalized values required for autoFocusPointOfInterest
      drawRectPosition: {
        x: Dimensions.get('window').width * 0.5 - 32,
        y: Dimensions.get('window').height * 0.5 - 32,
      },
    },
    focusDepth: 0,
    type: 'back',
    whiteBalance: 'auto',
    ratio: '16:9',
  
    isRecording: false,
    canDetectFaces: false,
    canDetectText: false,
    canDetectBarcode: false,
    faces: [],
    textBlocks: [],
    barcodes: [],
  };
  
  const renderFaces = (faces) => (
    <View style={styles.facesContainer} pointerEvents="none">
      {faces.map(renderFace)}
    </View>
  );
  
  const renderFace = ({ bounds, faceID, rollAngle, yawAngle }) => (
    <View
      key={faceID}
      transform={[
        { perspective: 600 },
        { rotateZ: `${rollAngle.toFixed(0)}deg` },
        { rotateY: `${yawAngle.toFixed(0)}deg` },
      ]}
      style={[
        styles.face,
        {
          ...bounds.size,
          left: bounds.origin.x,
          top: bounds.origin.y,
        },
      ]}>
      {/* <Text style={styles.faceText}>ID: {faceID}</Text>
      <Text style={styles.faceText}>rollAngle: {rollAngle.toFixed(0)}</Text>
      <Text style={styles.faceText}>yawAngle: {yawAngle.toFixed(0)}</Text> */}
    </View>
  );
  
  const renderLandmarksOfFace = (face = {}) => {
    const renderLandmark = (position) =>
      position && (
        <View
          style={[
            styles.landmark,
            {
              left: position.x - landmarkSize / 2,
              top: position.y - landmarkSize / 2,
            },
          ]}
        />
      );
    return (
      <View key={`landmarks-${face.faceID}`}>
        {renderLandmark(face.leftEyePosition)}
        {renderLandmark(face.rightEyePosition)}
        {renderLandmark(face.leftEarPosition)}
        {renderLandmark(face.rightEarPosition)}
        {renderLandmark(face.leftCheekPosition)}
        {renderLandmark(face.rightCheekPosition)}
        {renderLandmark(face.leftMouthPosition)}
        {renderLandmark(face.mouthPosition)}
        {renderLandmark(face.rightMouthPosition)}
        {renderLandmark(face.noseBasePosition)}
        {renderLandmark(face.bottomMouthPosition)}
      </View>
    );
  };
  
  const renderLandmarks = (faces = []) => (
    <View style={styles.facesContainer} pointerEvents="none">
      {faces.map(renderLandmarksOfFace)}
    </View>
  );
  
  const renderTextBlocks = (textBlocks = []) => (
    <View style={styles.facesContainer} pointerEvents="none">
      {textBlocks.map(renderTextBlock)}
    </View>
  );
  
  const renderTextBlock = ({ bounds = {}, value }) => (
    <React.Fragment key={value + bounds.origin.x}>
      <Text
        style={[
          styles.textBlock,
          { left: bounds.origin.x, top: bounds.origin.y },
        ]}>
        {value}
      </Text>
      <View
        style={[
          styles.text,
          {
            ...bounds.size,
            left: bounds.origin.x,
            top: bounds.origin.y,
          },
        ]}
      />
    </React.Fragment>
  );
  
  const renderBarcodes = (barcodes = []) => (
    <View style={styles.facesContainer} pointerEvents="none">
      {barcodes.map(renderBarcode)}
    </View>
  );
  
  const renderBarcode = ({ bounds = {}, data, type }) => (
    <React.Fragment key={data + bounds.origin.x}>
      <View
        style={[
          styles.text,
          {
            ...bounds.size,
            left: bounds.origin.x,
            top: bounds.origin.y,
          },
        ]}>
        <Text style={[styles.textBlock]}>{`${data} ${type}`}</Text>
      </View>
    </React.Fragment>
  );

export default function Camera({props}){
    const {onSelect} = route.params
    const {t} = useTranslation();
    const { colors } = useTheme()
    const [ data, setData ] = useState('')
    const [modalVisible, setModalVisible] = useState(false)
    const {onPress} = props
    const [
        {
          cameraRef,
          type,
          flash,
          autoFocus,
          focusDepth,
          zoom,
          whiteBalance,
          autoFocusPoint,
          drawFocusRingPosition,
          barcodes,
          ratio,
          cameraState,
          isRecording,
          faces,
          textBlocks,
        },
        {
          toggleFacing,
          toggleFlash,
          toggleAutoFocus,
          setFocusDepth,
          toggleWB,
          touchToFocus,
          toggleCameraState,
          facesDetected,
          textRecognized,
          barcodeRecognized,
          zoomIn,
          zoomOut,
          setIsRecording,
          takePicture,
          recordVideo,
        },
      ] = useCamera(initialState);

      //TODO: [mr] useEffect?
  const canDetectFaces = useMemo(() => cameraState['canDetectFaces'], [
    cameraState,
  ]);
  const canDetectText = useMemo(() => cameraState['canDetectText'], [
    cameraState,
  ]);
  const canDetectBarcode = useMemo(() => cameraState['canDetectBarcode'], [
    cameraState,
  ]);
    
  const onSelectData = (item) => {
    setData(item)
    setModalVisible(true)
  }

  const onSubmit = () => {
    setModalVisible(false)
    onSelect({item})
    navigation.goBack()
  }
  
  return (
    <View style={styles.container}>
      <Modal
        isVisible={modalVisible}
        backdropColor="rgba(0, 0, 0, 0.5)"
        backdropOpacity={1}
        animationIn="fadeIn"
        animationInTiming={600}
        animationOutTiming={600}
        backdropTransitionInTiming={600}
        backdropTransitionOutTiming={600}>
        <View style={{width: "100%", backgroundColor: colors.card}}>
          <Image resizeMode="contain"
              source={{uri: data.uri}} style={styles.thumb} />
          <View style={styles.contentActionCalendar}>
              <TouchableOpacity
                onPress={() => {
                  setData('')
                  setModalVisible(false)
                }}>
                <Text body1>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {onSubmit()}}>
                <Text body1 primaryColor>
                  {t('done')}
                </Text>
              </TouchableOpacity>
            </View>
        </View>
      </Modal>
        <RNCamera
          ref={cameraRef}
          style={{
            flex: 1,
            justifyContent: 'space-between',
          }}
          type={type}
          flashMode={flash}
          autoFocus={autoFocus}
          autoFocusPointOfInterest={autoFocusPoint.normalized}
          zoom={zoom}
          whiteBalance={whiteBalance}
          ratio={ratio}
          focusDepth={focusDepth}
          permissionDialogTitle={'Permission to use camera'}
          permissionDialogMessage={
            'We need your permission to use your camera phone'
          }
          faceDetectionLandmarks={
            RNCamera.Constants.FaceDetection.Landmarks
              ? RNCamera.Constants.FaceDetection.Landmarks.all
              : null
          }
          onFacesDetected={canDetectFaces ? facesDetected : null}
          onTextRecognized={canDetectText ? textRecognized : null}
          onGoogleVisionBarcodesDetected={
            canDetectBarcode ? barcodeRecognized : null
          }>
          <View style={StyleSheet.absoluteFill}>
            <View style={[styles.autoFocusBox, drawFocusRingPosition]} />
            <TouchableWithoutFeedback onPress={touchToFocus}>
              <View style={{ flex: 1 }} />
            </TouchableWithoutFeedback>
          </View>
          <View
            style={{
              height: 72,
              backgroundColor: 'transparent',
              justifyContent: 'space-around',
            }}>
            <View 
              style={{padding: 10, flexDirection: 'row',justifyContent: 'space-between',}}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <Icon
                    name="times"
                    size={25}
                    color={'#fff'}
                    enableRTL={true}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <Icon
                    name="bolt"
                    size={25}
                    color={'#fff'}
                    enableRTL={true}
                  />
                </TouchableOpacity>
            </View>
            <View
              style={{
                backgroundColor: 'transparent',
                flexDirection: 'row',
                justifyContent: 'space-around',
                margin: 10,
                marginTop: 20
              }}>
                
                
              {/* <TouchableOpacity
                style={styles.flipButton}
                onPress={() => toggleFacing()}>
                <Text style={styles.flipText}> FLIP </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.flipButton}
                onPress={() => toggleFlash()}>
                <Text style={styles.flipText}> FLASH: {flash} </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.flipButton}
                onPress={() => toggleWB()}>
                <Text style={styles.flipText}> WB: {whiteBalance} </Text>
              </TouchableOpacity> */}
            </View>
            <View
              style={{
                backgroundColor: 'transparent',
                flexDirection: 'row',
                justifyContent: 'space-around',
              }}>
              <TouchableOpacity
                onPress={() => toggleCameraState('canDetectFaces')}
                style={styles.flipButton}>
                <Text style={styles.flipText}>
                  {!canDetectFaces ? 'Detect Faces' : 'Detecting Faces'}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => toggleCameraState('canDetectText')}
                style={styles.flipButton}>
                <Text style={styles.flipText}>
                  {!canDetectText ? 'Detect Text' : 'Detecting Text'}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => toggleCameraState('canDetectBarcode')}
                style={styles.flipButton}>
                <Text style={styles.flipText}>
                  {!canDetectBarcode ? 'Detect Barcode' : 'Detecting Barcode'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ bottom: 0 }}>
            <View style={{padding: 10, flexDirection: 'row',justifyContent: 'space-between',}}>
              <TouchableOpacity>
                <View style={{height: 50, width: 50}}>
                {/* <Icon
                  name="times"
                  size={25}
                  color={'#fff'}
                  enableRTL={true}
                /> */}
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={async () => {
                try {
                  setIsRecording(true);
                  const data = await takePicture();
                  onSelectData(data)
                } catch (e) {
                  console.warn(e);
                } finally {
                  setIsRecording(false);
                }
              }}>
                <Icon
                  name="circle-notch"
                  size={65}
                  color={'#fff'}
                  enableRTL={true}
                />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => toggleFacing()}>
                <Icon
                  name="undo-alt"
                  size={25}
                  color={'#fff'}
                  enableRTL={true}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: 20,
                backgroundColor: 'transparent',
                flexDirection: 'row',
                alignSelf: 'flex-end',
              }}>
              {/* <Slider
                style={{ width: 150, marginTop: 15, alignSelf: 'flex-end' }}
                onValueChange={(value = 0) => setFocusDepth(value)}
                step={0.1}
                disabled={autoFocus === 'on'}
              /> */}
            </View>
            {/* <View
              style={{
                height: 56,
                backgroundColor: 'transparent',
                flexDirection: 'row',
                alignSelf: 'flex-end',
              }}>
              <TouchableOpacity
                style={[
                  styles.flipButton,
                  {
                    flex: 0.3,
                    alignSelf: 'flex-end',
                    backgroundColor: isRecording ? 'white' : 'darkred',
                  },
                ]}
                onPress={
                  isRecording
                    ? () => {}
                    : async () => {
                        try {
                          setIsRecording(true);
                          const data = await recordVideo(recordOptions);
                          console.warn(data);
                        } catch (error) {
                          console.warn(error);
                        } finally {
                          setIsRecording(false);
                        }
                      }
                }>
                {isRecording ? (
                  <Text style={styles.flipText}> ☕ </Text>
                ) : (
                  <Text style={styles.flipText}> REC </Text>
                )}
              </TouchableOpacity>
            </View> */}
            {/* {zoom !== 0 && (
              <Text style={[styles.flipText, styles.zoomText]}>Zoom: {zoom}</Text>
            )}
            <View
              style={{
                height: 56,
                backgroundColor: 'transparent',
                flexDirection: 'row',
                alignSelf: 'flex-end',
              }}>
              <TouchableOpacity
                style={[styles.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
                onPress={zoomIn}>
                <Text style={styles.flipText}> + </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.flipButton, { flex: 0.1, alignSelf: 'flex-end' }]}
                onPress={zoomOut}>
                <Text style={styles.flipText}> - </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.flipButton, { flex: 0.25, alignSelf: 'flex-end' }]}
                onPress={toggleAutoFocus}>
                <Text style={styles.flipText}> AF : {autoFocus} </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.flipButton,
                  styles.picButton,
                  { flex: 0.3, alignSelf: 'flex-end' },
                ]}
                onPress={async () => {
                  try {
                    setIsRecording(true);
                    const data = await takePicture();
                    console.warn(data);
                  } catch (e) {
                    console.warn(e);
                  } finally {
                    setIsRecording(false);
                  }
                }}>
                <Text style={styles.flipText}> SNAP </Text>
              </TouchableOpacity>
            </View> */}
          </View>
          {!!canDetectFaces && renderFaces(faces)}
          {!!canDetectFaces && renderLandmarks(faces)}
          {!!canDetectText && renderTextBlocks(textBlocks)}
          {!!canDetectBarcode && renderBarcodes(barcodes)}
        </RNCamera>
      </View>
    );
  };

  Camera.propTypes = {
    onPress: PropTypes.func,
  }
  
  Camera.defaultProps = {
    onPress: () => {},
  }