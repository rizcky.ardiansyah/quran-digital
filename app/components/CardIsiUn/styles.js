import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  imageBanner: {flex: 1, borderRadius: 8},
  card: {
    // width: '100%',
    height: '100%',
    borderRadius: 8,
  },
  content: {
    // position: 'absolute',
    alignItems: 'center',
    bottom: 0,
    // marginTop: 20,
    padding: 5,
    justifyContent: 'center',
    alignContent: 'center',
    flexDirection: 'row',
    margin: 20
  },
  iconContent: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    borderRadius: 15,
  },
});
