import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import styles from './styles'
import PropTypes from 'prop-types'
import {Image, Icon, Text} from '@components';
import { Images, useTheme } from '@config'

export default function Card(props) {
	const { colors } = useTheme()
	const {
		style,
		children,
		styleContent,
		image,
		onPress,
		judul,
		warna,
	} = props
	return (
		<TouchableOpacity
			style={[
				styles.card,
				{ borderColor: colors.primary, backgroundColor: 'white' },
				style,
			]}
			onPress={onPress}
			activeOpacity={0.9}>
			<View
				style={[styles.iconContent, { backgroundColor: colors.card }]}>
				<View style={[styles.content]}>
					<Icon name={image} size={20} color={warna} solid />
					<View style={{justifyContent: 'center', marginLeft: 15}}>
						<Text body>{'Request'}</Text>
						<Text body bold>{judul}</Text>
					</View>
				</View>
				{/* <View style={[{ paddingTop: 0 }]}>
					<Text header>{warna}</Text>
				</View> */}
			</View>
		</TouchableOpacity>
	)
}

Card.propTypes = {
	image: PropTypes.node.isRequired,
	judul: PropTypes.string,
	warna: PropTypes.string,
	style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
	styleContent: PropTypes.object,
	children: PropTypes.oneOfType([
		PropTypes.element,
		PropTypes.arrayOf(PropTypes.element),
	]),
	onPress: PropTypes.func,
}

Card.defaultProps = {
	image: Images.profile2,
	judul: null,
	warna: null,
	style: {},
	styleContent: {},
	onPress: () => {},
}
