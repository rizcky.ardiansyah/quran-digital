import React, { useState } from 'react'
import { View, TouchableOpacity } from 'react-native'
import { Image, Text, Icon } from '@components'
import styles from './styles'
import PropTypes from 'prop-types'
import { useTheme } from '@config'
import CheckBox from '@react-native-community/checkbox'

export default function ListThumbNilai(props) {
	const { colors } = useTheme()
	const [toggleCheckBox, setToggleCheckBox] = useState(false)
	const {
		style,
		imageStyle,
		image,
		txtLeft,
		txtLeftTitle,
		txtContent,
		txtRight,
		txtRightContent,
		onPress,
		warna,
		checkView,
		txtRightStatus,
		onCheck,
		item
	} = props
	return (
		<TouchableOpacity
			style={[
				styles.contain,
				{ borderBottomWidth: 1, borderBottomColor: colors.border },
				style,
			]}
			onPress={onPress}
			activeOpacity={0.9}>
			<View style={styles.content}>
				<View style={styles.left}>
					<Text bold semibold style={{marginRight: 5}}>
						{txtLeft}
					</Text>
					<Text note semibold style={{marginRight: 5}}>
						{txtLeftTitle}
					</Text>
					<Text
						note
						numberOfLines={1}
						semibold
						primaryColor
						>
						{txtContent}
					</Text>
				</View>
				<View style={styles.right}>
					<Text bold semibold>{txtRightStatus}</Text>
					<Text caption2 grayColor numberOfLines={1} style={{paddingTop: 5}}>
						{txtRight}
					</Text>
				</View>
			</View>
		</TouchableOpacity>
	)
}

ListThumbNilai.propTypes = {
	style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
	imageStyle: PropTypes.object,
	item: PropTypes.object,
	image: PropTypes.node.isRequired,
	txtLeftTitle: PropTypes.string,
	txtContent: PropTypes.string,
	txtRight: PropTypes.string,
	txtRightContent: PropTypes.string,
	txtRightStatus: PropTypes.string,
	onPress: PropTypes.func,
	onCheck: PropTypes.func,
	txtLeft: PropTypes.string,
	warna: PropTypes.string,
	checkView: PropTypes.string
}

ListThumbNilai.defaultProps = {
	style: {},
	item: {},
	imageStyle: {},
	image: '',
	txtLeft: '',
	txtLeftTitle: '',
	txtContent: '',
	txtRight: '',
	txtRightContent: '',
	txtRightStatus: '',
	warna: '',
	checkView: '',
	onPress: () => {},
	onCheck: () => {}
}
