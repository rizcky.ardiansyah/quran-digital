import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import {Text, Icon} from '@components';
import styles from './styles';
import {Calendar} from 'react-native-calendars';
import Modal from 'react-native-modal';
import {BaseColor, useTheme, DefaultFont} from '@config';
import {useTranslation} from 'react-i18next';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

export default function TimePicker(props) {
  const {t} = useTranslation();
  const {colors} = useTheme();
  const [date, setDate] = useState(new Date(1598051730000));
  const [selected, setSelected] = useState(props.selected);
  const [markedDates, setMarkedDates] = useState({});
  const [modalVisible, setModalVisible] = useState(false);
  const [renderCalendar, setRenderCalendar] = useState(false);
  const [show, setShow] = useState(false);

  useEffect(() => {
    setRenderCalendar(false);
    // setTimeout(() => {
    //   setRenderCalendar(true);
    // }, 250);
  }, [colors.card]);

  useEffect(() => {
    let marked = {};
    marked[selected] = {
      selected: true,
      marked: true,
      selectedColor: colors.primary,
    };
    setMarkedDates(marked);
  }, []);

  const setDaySelected = selected => {
    // console.log("setDaySelected : "+selected)
    let marked = {};
    marked[selected] = {
      selected: true,
      marked: true,
      selectedColor: colors.primary,
    };
    setMarkedDates(marked);
    setSelected(selected);
  };
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    console.log("showDatePicker")
    // setDatePickerVisibility(true);
    setRenderCalendar(true)
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    console.warn("A date has been picked: ", date);
    hideDatePicker();
  };

  const onRubah = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    // setShow(Platform.OS === 'ios');
    setRenderCalendar(false)
    setDate(currentDate);
    let dt = moment(selectedDate).format('HH:mm')
    setSelected(dt+":00");
    onChange(dt+":00");
    console.log("onRubah "+JSON.stringify(selectedDate) +", dt : "+dt)
  };

  const {style, label, minDate, maxDate, onCancel, onChange} = props;

  return (
    <View
      style={[styles.contentPickDate, {backgroundColor: colors.card}, style]}>
      {/* <Modal
        isVisible={modalVisible}
        backdropColor="rgba(0, 0, 0, 0.5)"
        backdropOpacity={1}
        animationIn="fadeIn"
        animationInTiming={600}
        animationOutTiming={600}
        backdropTransitionInTiming={600}
        backdropTransitionOutTiming={600}>
        <View style={[styles.contentCalendar, {backgroundColor: colors.card}]}>
          {renderCalendar && (
            <Calendar
              style={{
                borderRadius: 8,
                backgroundColor: colors.card,
              }}
              renderArrow={direction => {
                return (
                  <Icon
                    name={direction == 'left' ? 'angle-left' : 'angle-right'}
                    size={14}
                    color={colors.primary}
                    enableRTL={true}
                  />
                );
              }}
              markedDates={markedDates}
              current={selected}
              minDate={minDate}
              maxDate={maxDate}
              onDayPress={day => setDaySelected(day.dateString)}
              monthFormat={'dd-MM-yyyy'}
              theme={{
                calendarBackground: colors.card,
                textSectionTitleColor: colors.text,
                selectedDayBackgroundColor: colors.primary,
                selectedDayTextColor: '#ffffff',
                todayTextColor: colors.primary,
                dayTextColor: colors.text,
                textDisabledColor: BaseColor.grayColor,
                dotColor: colors.primary,
                selectedDotColor: '#ffffff',
                arrowColor: colors.primary,
                monthTextColor: colors.text,
                textDayFontFamily: DefaultFont,
                textMonthFontFamily: DefaultFont,
                textDayHeaderFontFamily: DefaultFont,
                textMonthFontWeight: 'bold',
                textDayFontSize: 14,
                textMonthFontSize: 16,
                textDayHeaderFontSize: 14,
              }}
            />
          )}
          <View style={styles.contentActionCalendar}>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(false);
                setSelected(props.selected);
                onCancel();
              }}>
              <Text body1>{t('cancel')}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(false);
                onChange(selected);
              }}>
              <Text body1 primaryColor>
                {t('done')}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal> */}
      {renderCalendar && (
      <DateTimePicker
              testID="dateTimePicker"
              value={date}
              mode={'time'}
              is24Hour={true}
              display="default"
              onChange={onRubah}
            />
            )}
      <TouchableOpacity
        style={styles.itemPick}
        onPress={() => setRenderCalendar(true)}>
        <Text caption1 light style={{marginBottom: 5}}>
          {label}
        </Text>
        <Text headline semibold numberOfLines={1}>
          {selected}
        </Text>
      </TouchableOpacity>
    </View>
  );
}

TimePicker.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  selected: PropTypes.string,
  label: PropTypes.string,
  minDate: PropTypes.string,
  maxDate: PropTypes.string,
  onCancel: PropTypes.func,
  onChange: PropTypes.func,
};

TimePicker.defaultProps = {
  style: {},
  selected: '',
  label: 'Label',
  minDate: '2021-01-01',
  maxDate: '2025-12-31',
  onCancel: () => {},
  onChange: () => {},
};
