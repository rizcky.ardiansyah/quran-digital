import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import { Image, Text, Icon } from '@components'
import styles from './styles'
import PropTypes from 'prop-types'
import { useTheme } from '@config'
export default function ListThumbCircle(props) {
	const { colors } = useTheme()
	const {
		style,
		imageStyle,
		image,
		txtLeftTitle,
		txtContent,
		txtRight,
		txtRightContent,
		onPress,
		txtStatus,
	} = props
	return (
		<TouchableOpacity
			style={[
				styles.contain,
				{ borderBottomWidth: 1, borderBottomColor: colors.border },
				style,
			]}
			onPress={onPress}
			activeOpacity={0.9}>
			{image === '' ? (
				<Icon
					name="user"
					size={20}
					color={colors.primary}
					enableRTL={true}
					style={{ paddingRight: 15 }}
				/>
			) : (
				<Image source={image} style={[styles.thumb, imageStyle]} />
			)}
			<View style={styles.content}>
				<View style={styles.left}>
					<Text note semibold>
						{txtLeftTitle}
					</Text>
					<View
						style={{
							// flex: 8,
							flexDirection: 'row',
							justifyContent: 'space-between',
							paddingTop: 5,
							width: '100%',
						}}>
						<View style={{ flexDirection: 'row' }}>
							<Icon
								name="user"
								size={10}
								color={colors.primary}
								enableRTL={true}
								style={{ paddingRight: 10, paddingTop: 3 }}
							/>
							<Text note numberOfLines={1} footnote grayColor>
								{txtContent}
							</Text>
						</View>
						<View
							style={{
								marginRight: 10,
								backgroundColor: colors.primary,
								paddingRight: 10,
								paddingLeft: 10,
								paddingBottom: 5,
								borderRadius: 10,
							}}>
							<Text style={{ fontColor: '#fff' }}>
								{txtStatus}
							</Text>
						</View>
					</View>
				</View>
				<View style={styles.right}>
					<Text caption2 grayColor numberOfLines={1}>
						{txtRight}
					</Text>
					<Text
						caption2
						grayColor
						numberOfLines={1}
						style={{
							paddingTop: 5,
						}}>
						{txtRightContent}
					</Text>
				</View>
			</View>
		</TouchableOpacity>
	)
}

ListThumbCircle.propTypes = {
	style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
	imageStyle: PropTypes.object,
	image: PropTypes.node.isRequired,
	txtLeftTitle: PropTypes.string,
	txtContent: PropTypes.string,
	txtRight: PropTypes.string,
	txtRightContent: PropTypes.string,
	onPress: PropTypes.func,
	txtStatus: PropTypes.string,
}

ListThumbCircle.defaultProps = {
	style: {},
	imageStyle: {},
	image: '',
	txtLeftTitle: '',
	txtContent: '',
	txtRight: '',
	txtRightContent: '',
	txtStatus: '',
	onPress: () => {},
}
