import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  imageBanner: {flex: 1, borderRadius: 8},
  card: {
    width: '100%',
    height: '100%',
    // borderRadius: 8,
  },
  content: {
    position: 'absolute',
    alignItems: 'center',
    bottom: 0,
    padding: 10,
    justifyContent: 'center',
    alignContent: 'center',
  },
  iconContent: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    // borderRadius: 15,
  },
});
