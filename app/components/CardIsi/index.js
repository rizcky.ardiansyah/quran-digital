import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import {Image, Icon} from '@components';
import {Images, useTheme} from '@config';

export default function Card(props) {
  const {colors} = useTheme();
  const {style, children, styleContent, image, onPress, judul, warna} = props;
  return (
    <TouchableOpacity
      style={[
        styles.card,
        {borderColor: colors.primary, },
        style,
      ]}
      onPress={onPress}
      activeOpacity={0.9}>
      <View style={[styles.iconContent, {backgroundColor: colors.card}]}>
        <View style={[styles.content, {paddingTop: 50}]}>
          <Icon name={image} size={40} color={warna} solid />
          <Text body2>{judul}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

Card.propTypes = {
  image: PropTypes.node.isRequired,
  judul: PropTypes.string,
  warna: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  styleContent: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  onPress: PropTypes.func,
};

Card.defaultProps = {
  image: Images.profile2,
  judul: null,
  warna: null,
  style: {},
  styleContent: {},
  onPress: () => {},
};
