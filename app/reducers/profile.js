import * as actionTypes from '@actions/actionTypes'
const initialState = {
	profil: null,
}

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case actionTypes.PROFILE:
			return {
				...state,
				profil: action.profil,
			}
		default:
			return state
	}
}
