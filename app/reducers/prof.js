import * as actionTypes from '@actions/actionTypes'
const initialState = {
	prof: null,
	token: null,
	profdetail: null,
	img: null,
	gambar: null,
	gita: null,
	sign: null,
}

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case actionTypes.PROFILE:
			return { ...state, prof: action.prof }
		case actionTypes.GET_PROFILE:
			return { ...state, profdetail: action.profdetail }
		case actionTypes.TOKEN:
			return { ...state, token: action.token }
		case actionTypes.IMAGE:
			return { ...state, img: action.img }
		case actionTypes.GAMBAR:
			return { ...state, gambar: action.gambar }
		case actionTypes.GITA:
			return { ...state, gita: action.gita }
		case actionTypes.SIGN:
			return { ...state, sign: action.sign }
		default:
			return state
	}
}
