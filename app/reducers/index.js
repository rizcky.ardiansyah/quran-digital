import {combineReducers} from 'redux';
import AuthReducer from './auth';
import ApplicationReducer from './application';
import ProfileReducer from './prof';

export default combineReducers({
  auth: AuthReducer,
  application: ApplicationReducer,
  prof: ProfileReducer,
});
