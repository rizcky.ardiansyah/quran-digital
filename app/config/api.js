import { StyleSheet } from 'react-native'

/**
 * Common basic style defines
 */
export const Api = {
	mainUrl		: 'http://api.alquran.cloud/v1/',
	surahUrl 	: 'surah',
	ayatUrl 	: 'ayah',
	searchUrl 	: 'search',
	manzilUrl	: 'manzil',
	rukuUrl		: 'ruku',
	pageUrl		: 'page',
	hizbUrl		: 'hizbQuarter',
	sajdaUrl	: 'sajda',
	metaUrl		: 'meta',
	editionUrl	: 'edition'
}
