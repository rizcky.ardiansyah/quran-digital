import {useState, useEffect} from 'react';
import DeviceInfo from 'react-native-device-info';
import {
  getManufacturer,
  getManufacturerSync,
  syncUniqueId,
  useBatteryLevel,
  useBatteryLevelIsLow,
  usePowerState,
  useFirstInstallTime,
  useDeviceName,
  useHasSystemFeature,
  useIsEmulator,
} from 'react-native-device-info';

/**
 * export font for application
 * @returns font
 */
export default function DevInfo() {
  // const batteryLevel = useBatteryLevel();
  // const batteryLevelIsLow = useBatteryLevelIsLow();
  // const powerState = usePowerState();
  // const firstInstallTime = useFirstInstallTime();
  // const deviceName = useDeviceName();
  // const hasSystemFeature = useHasSystemFeature('amazon.hardware.fire_tv');
  // const isEmulator = useIsEmulator();
  // // console.log('device Name : ' + JSON.stringify(deviceName));
  // const deviceJSON = {
  //   batteryLevel,
  //   batteryLevelIsLow,
  //   powerState,
  //   firstInstallTime,
  //   deviceName,
  //   hasSystemFeature,
  //   isEmulator,
  // };
  // const [data, setData] = useState([]);
  // useEffect(() => {
  //   setData(deviceJSON);
  //   console.log('DevInfo: ' + JSON.stringify(data));
  // }, []);
  const FunctionalComponent = () => {
    const batteryLevel = useBatteryLevel();
    const batteryLevelIsLow = useBatteryLevelIsLow();
    const powerState = usePowerState();
    const firstInstallTime = useFirstInstallTime();
    const deviceName = useDeviceName();
    const hasSystemFeature = useHasSystemFeature('amazon.hardware.fire_tv');
    const isEmulator = useIsEmulator();
    const deviceJSON = {
      batteryLevel,
      batteryLevelIsLow,
      powerState,
      firstInstallTime,
      deviceName,
      hasSystemFeature,
      isEmulator,
    };

    return deviceJSON;
  };
  return FunctionalComponent;
}
