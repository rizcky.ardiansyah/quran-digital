import React, { useState } from 'react'
import {
	View,
	ScrollView,
	TouchableOpacity,
	PermissionsAndroid,
} from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { AuthActions } from '@actions'
import { BaseStyle, useTheme, Api, Images } from '@config'
import {
	Header,
	SafeAreaView,
	Icon,
	Text,
	Button,
	ProfileDetail,
	ProfilePerformance, Image
} from '@components'
import styles from './styles'
import { useTranslation } from 'react-i18next'

export default function Utama({ navigation }) {
	const { colors } = useTheme()
	const { t } = useTranslation()

	const [loading, setLoading] = useState(false)
	const dispatch = useDispatch()

	const profile = useSelector(state => state.prof)
	// console.log('profile : '+JSON.stringify(profile))
	/**
	 * @description Simple logout with Redux
	 * @author Passion UI <passionui.com>
	 * @date 2019-08-03
	 */
	const onLogOut = () => {
		setLoading(true)
		console.log('logout processing')
		dispatch(
			AuthActions.authentication(false, response => {
				navigation.navigate('Home')
			}),
		)
	}

	return (
		<SafeAreaView
			style={BaseStyle.safeAreaView}
			forceInset={{ top: 'always' }}>
			<Header
				title={'Quran Digital'}
				// renderRight={() => {
				//   return <Icon name="sun" size={24} color={colors.primary} />;
				// }}
				// onPressRight={() => {
				//   navigation.navigate('Notification');
				// }}
			/>
			<ScrollView>
				<View style={styles.contain}>
					<View style={{ marginBottom: 50, alignItems: 'center' }}>
						<Image
							source={Images.logo}
							style={styles.logo}
							resizeMode="contain"
						/>
					</View>
					<TouchableOpacity
						style={[
							styles.profileItem,
							{
								borderBottomColor: colors.border,
								borderBottomWidth: 1,
							},
						]}
						onPress={() => {
							navigation.navigate('Quran')
						}}>
						<View style={{ flexDirection: 'row'}}>
						{/* <Icon
							name="user-circle"
							size={18}
							color={colors.primary}
							style={{ marginLeft: 5, marginRight: 10 }}
							enableRTL={true}
						/> */}
						<Text body1>{t('read')}</Text>
						</View>
						{/* <Icon
							name="angle-right"
							size={18}
							color={colors.primary}
							style={{ marginLeft: 5 }}
							enableRTL={true}
						/> */}
					</TouchableOpacity>
					<TouchableOpacity
						style={[
							styles.profileItem,
							{
								borderBottomColor: colors.border,
								borderBottomWidth: 1,
							},
						]}
						onPress={() => {
							navigation.navigate('ChangePassword')
						}}>
						<View style={{ flexDirection: 'row'}}>
						{/* <Icon
							name="lock"
							size={18}
							color={colors.primary}
							style={{ marginLeft: 5, marginRight: 10 }}
							enableRTL={true}
						/> */}
						<Text body1>{t('search')}</Text>
						</View>
						{/* <Icon
							name="angle-right"
							size={18}
							color={colors.primary}
							style={{ marginLeft: 5 }}
							enableRTL={true}
						/> */}
					</TouchableOpacity>
					<TouchableOpacity
						style={[
							styles.profileItem,
							{
								borderBottomColor: colors.border,
								borderBottomWidth: 1,
							},
						]}
						onPress={() => {
							navigation.navigate('Setting')
						}}>
						<View style={{ flexDirection: 'row'}}>
						{/* <Icon
							name="star-of-life"
							size={18}
							color={colors.primary}
							style={{ marginLeft: 5, marginRight: 10 }}
							enableRTL={true}
						/> */}
						<Text body1>{t('setting')}</Text>
						</View>
						{/* <Icon
							name="angle-right"
							size={18}
							color={colors.primary}
							style={{ marginLeft: 5 }}
							enableRTL={true}
						/> */}
					</TouchableOpacity>
				</View>
			</ScrollView>
		</SafeAreaView>
	)
}
