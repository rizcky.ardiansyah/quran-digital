import React, { useState, useEffect, useCallback } from 'react'
import {
	View,
	ScrollView,
	TouchableOpacity,
	PermissionsAndroid,FlatList,ActivityIndicator,RefreshControl
} from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { AuthActions } from '@actions'
import { BaseStyle, useTheme, Api, Images } from '@config'
import {
	Header,
	SafeAreaView,
	Icon,
	Text,
	Button,
	ProfileDetail,
	ProfilePerformance, Image,ListThumbCircle
} from '@components'
import styles from './styles'
import { useTranslation } from 'react-i18next'
import axios from 'axios'

export default function Quran({ navigation }) {
	const { colors } = useTheme()
	const { t } = useTranslation()
	const [did, setDid] = useState()
	const [notif, setNotif] = useState([])
	const [notifArr, setNotifArr] = useState([])
	const [loading, setLoading] = useState(true)
	const dispatch = useDispatch()
	const [refreshing] = useState(false)
	const keyExtractor = useCallback(item => item.id.toString(), [])

 	const getSurah = () => {
	console.log("getSurah")
	let url = Api.mainUrl + '/' + Api.surahUrl 
	axios({
		method: 'get',
		url: url
	})
		.then(function(resp) {
			console.log('function get Surah --> ' + JSON.stringify(resp.data.data))
			if (resp.data.status == 'OK') {
				setNotif(resp.data.data)
				setNotifArr(resp.data.data)
				setDid('1')
			}
			console.log("notifikasi : "+JSON.stringify(notif));
			setLoading(false)
		})
		.catch(error => {
			setLoading(false)
			console.log(
				'error api project notification = ' + JSON.stringify(error),
			)
			if (error.response) {
				Alert.alert(
					'Alert',
					error.message,
					[{ text: 'OK', onPress: () => onOK() }],
					{ cancelable: false },
				)
			} else if (error.request) {
				Alert.alert(
					'Alert',
					error.request,
					[{ text: 'OK', onPress: () => onOK() }],
					{ cancelable: false },
				)
			} else {
				Alert.alert(
					'Alert',
					error.message,
					[{ text: 'OK', onPress: () => onOK() }],
					{ cancelable: false },
				)
			}
		})
	}

	const onPilih = item => {
		navigation.navigate('QuranRead', { item })
	}

	useEffect(() => {
		getSurah()
	}, [did])

	const renderItem = function({ item }) {
		return (
			<ListThumbCircle
				// image={renderImage(item)}
				// warna={renderWarna(item)}
				item={item}
				txtLeft={item.englishName}
				txtLeftTitle={item.revelationType + " | "+item.numberOfAyahs +" Ayat"}
				// txtContent={item.englishName}
				txtRight={item.name}
				// txtRightContent={item.waktu.split(' ')[1]}
				style={{ marginBottom: 5 }}
				// checkView={
				// 	item.dari_nik != profile.prof[0].nik &&
				// 	item.progres.id == '0' &&
				// 	item.dari != 'ATTENDANCE'
				// 		? item.id
				// 		: ''
				// }
				onPress={() => {
					onPilih(item)
				}}
			/>
		)
	}

	return (
		<SafeAreaView
			style={BaseStyle.safeAreaView}
			forceInset={{ top: 'always' }}>
			<Header
				title={t('read')}
				renderLeft={() => {
					return (
						<Icon
							name="arrow-left"
							size={20}
							color={colors.primary}
							enableRTL={true}
						/>
					)
				}}
				onPressLeft={() => {
					navigation.goBack()
				}}
			/>
			{loading ? (
				<ActivityIndicator
					size="small"
					color={colors.primary}
					style={{ paddingLeft: 5 }}
				/>
			) : (
				<FlatList
					removeClippedSubviews
					// ListHeaderComponent={getHeader}
					contentContainerStyle={{
						paddingHorizontal: 20,
						paddingVertical: 10,
					}}
					refreshControl={
						<RefreshControl
							colors={[colors.primary]}
							tintColor={colors.primary}
							refreshing={refreshing}
							onRefresh={() => {
								onRefresh()
							}}
						/>
					}
					data={notif}
					keyExtractor={item => item.number}
					renderItem={renderItem}
					initialNumToRender={5}
					maxToRenderPerBatch={10}
				/>
			)}
		</SafeAreaView>
	)
}
