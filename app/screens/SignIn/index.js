import React, { useState } from 'react'
import { AuthActions, ProfileActions } from '@actions'
import {
	View,
	TouchableOpacity,
	KeyboardAvoidingView,
	Platform,
	Alert,
} from 'react-native'
import { BaseStyle, useTheme, Api, BaseColor, Images } from '@config'
import {
	Header,
	SafeAreaView,
	Icon,
	Text,
	Button,
	TextInput,
	Image,
} from '@components'
import styles from './styles'
import { useTranslation } from 'react-i18next'
import axios from 'axios'
import { useSelector, useDispatch } from 'react-redux'

export default function SignIn({ navigation }) {
	const domain = useSelector(state => state.application.domain)
	const { colors } = useTheme()
	const { t } = useTranslation()
	const dispatch = useDispatch()
	const [images, setImages] = useState('')
	const [id, setId] = useState('')
	const [password, setPassword] = useState('')
	const [loading, setLoading] = useState(false)
	const [success, setSuccess] = useState({ id: true, password: true })
	const [iconPassword, setIconPassword] = useState({
		icEye: 'eye',
		showPassword: true,
	})
	const onOK = () => {
		setId('')
		setPassword('')
		setLoading(false)
	}
	// call when store profile
	const onChange = forceDarkMode => {
		dispatch(AuthActions.authentication(true, response => {}))
		dispatch(AuthActions.onSetProfile(forceDarkMode))
		setLoading(false)
		setId('')
		setPassword('')
		navigation.navigate('Tab')
	}

	/**
	 * call when action login
	 *
	 */
	const onLogin = () => {
		console.log('masuk login')
		if (id === '' || password === '') {
			alert('ID dan Password tidak boleh kosong')
			setSuccess({
				...success,
				id: false,
				password: false,
			})
		} else {
			setLoading(true)
			console.log('masuk else')
			let url = domain + '/' + Api.apiGtkUrl + '/auth/login2'
			console.log('ini url', url)
			let bodyFormData = new FormData()
			bodyFormData.append('nik', id)
			bodyFormData.append('password', password)
			console.log('ini body', bodyFormData)
			axios({
				method: 'post',
				url: url,
				data: bodyFormData,
				headers: { 'Content-Type': 'multipart/form-data' },
			})
				.then(function(resp) {
					console.log('respon : ' + JSON.stringify(resp))
					if (resp.data.status == '1') {
						console.log('ini login', resp.data.data_gita)
						getImageSlider()
						dispatch(ProfileActions.onSetProfile(resp.data.data))
						if (resp.data.data_gita == '') {
							console.log('gak ada gita')
						} else {
							dispatch(
								ProfileActions.onSetToken(
									resp.data.data[0].token,
								),
							)
							dispatch(
								ProfileActions.onSetGita(
									resp.data.data_gita[0],
								),
							)
							let url =
								Api.gitaMainUrl +
								'/' +
								Api.signAllUrl +
								'/' +
								resp.data.data_gita[0].id
							console.log('masuk url sign', url)
							axios({
								method: 'get',
								url: url,
								headers: {
									'Content-Type': 'multipart/form-data',
									appsession: resp.data.data_gita[0].token,
								},
							})
								.then(function(resp) {
									if (resp.data.status === '1') {
										console.log('hasil ttd', resp.data.data)
										dispatch(
											ProfileActions.onSetSign(
												resp.data.data,
											),
										)
									} else {
										console.log('gagal sign', resp)
									}
								})
								.catch(function(response) {
									console.log('hasil gagal', response)
								})
						}
					} else {
						Alert.alert(
							'Alert',
							resp.data.info,
							[{ text: 'OK', onPress: () => onOK() }],
							{ cancelable: false },
						)
					}
				})
				.catch(function(error) {
					console.log('error : ' + JSON.stringify(error))
					if (error.response) {
						Alert.alert(
							'Alert',
							error.response.data.message,
							[{ text: 'OK', onPress: () => onOK() }],
							{ cancelable: false },
						)
					} else if (error.request) {
						Alert.alert(
							'Alert',
							error.request,
							[{ text: 'OK', onPress: () => onOK() }],
							{ cancelable: false },
						)
					} else {
						Alert.alert(
							'Alert',
							error.message,
							[{ text: 'OK', onPress: () => onOK() }],
							{ cancelable: false },
						)
					}
					setLoading(false)
					setId('')
					setPassword('')
				})
		}
	}

	const getImageSlider = () => {
		let url =
			domain +
			'/apigrtk/index.php/web_service/api/90001/0/0/0/350ea7e2af60c9d3824791dd122272d8'

		axios
			.get(url)
			.then(response => {
				let banner = []
				// console.log("banner isi : "+JSON.stringify(response.data.info))
				if (response.data.rst == '1') {
					for (let i = 0; i < response.data.info.length; i++) {
						banner.push({
							image: response.data.info[i].illustration,
							desc: response.data.info[i].title,
						})
					}
					dispatch(ProfileActions.onSetImg(banner))
					dispatch(
						AuthActions.authentication(true, response => {
							setLoading(false)
							setId('')
							setPassword('')
							navigation.navigate('Home')
						}),
					)
				} else {
					Alert.alert(
						'Alert',
						response.data.info,
						[
							{
								text: 'OK',
								onPress: () => console.log('OK Pressed'),
							},
						],
						{ cancelable: false },
					)
				}
			})
			.catch(err => {
				//   console.log(' Error Login = ' + JSON.stringify(err) )
				setLoading(false)
				Alert.alert(
					'Alert',
					'Error Network ',
					[{ text: 'OK', onPress: () => console.log('OK Pressed') }],
					{ cancelable: false },
				)
			})
	}

	const onChangePwdTtype = () => {
		// console.log("onChangePwdTtype "+iconPassword.showPassword);
		if (iconPassword.showPassword == true)
			setIconPassword({ icEye: 'eye-slash', showPassword: false })
		else setIconPassword({ icEye: 'eye', showPassword: true })
	}

	const offsetKeyboard = Platform.select({
		ios: 0,
		android: 20,
	})
	return (
		<SafeAreaView
			style={BaseStyle.safeAreaView}
			forceInset={{ top: 'always' }}>
			{/* <Header
				title={t('sign_in')}
				renderLeft={() => {
					return (
						<Icon
							name="arrow-left"
							size={20}
							color={colors.primary}
							enableRTL={true}
						/>
					)
				}}
				onPressLeft={() => {
					navigation.goBack()
				}}
			/> */}
			<KeyboardAvoidingView
				behavior={Platform.OS === 'android' ? 'height' : 'padding'}
				keyboardVerticalOffset={offsetKeyboard}
				style={{ flex: 1 }}>
				<View style={styles.contain}>
					<View style={{ marginBottom: 50 }}>
						<Image
							source={Images.logo}
							style={styles.logo}
							resizeMode="contain"
						/>
					</View>
					<TextInput
						onChangeText={text => setId(text)}
						onFocus={() => {
							setSuccess({
								...success,
								id: true,
							})
						}}
						placeholder={t('input_id')}
						success={success.id}
						value={id}
					/>
					<TextInput
						style={{ marginTop: 10 }}
						onChangeText={text => setPassword(text)}
						onFocus={() => {
							setSuccess({
								...success,
								password: true,
							})
						}}
						placeholder={t('input_password')}
						secureTextEntry={iconPassword.showPassword}
						success={success.password}
						value={password}
						icon={
							<Icon
								name={iconPassword.icEye}
								size={14}
								solid
								color={BaseColor.grayColor}
								onPress={() => onChangePwdTtype()}
							/>
						}
					/>
					<Button
						style={{ marginTop: 20 }}
						full
						loading={loading}
						onPress={() => {
							onLogin()
						}}>
						{t('sign_in')}
					</Button>
					{/* <View style={{position:"absolute", bottom: 0, marginBottom: 20, justifyContent: 'center', alignContent: 'center'}}>
						<TouchableOpacity
							onPress={() => navigation.navigate('ResetPassword')}>
							<Text title grayColor style={{ marginTop: 25, textAlign: 'center'}}>
								{t('forgot_your_password')}
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => navigation.navigate('SignUp')}>
							<Text title grayColor style={{ marginTop: 15, textAlign: 'center' }}>
								{t('not_have_account')}
							</Text>
						</TouchableOpacity>
					</View> */}
				</View>
			</KeyboardAvoidingView>
		</SafeAreaView>
	)
}
