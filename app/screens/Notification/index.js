import React, { useState, useEffect, useCallback } from 'react'
import {
	View,
	Alert,
	RefreshControl,
	FlatList,
	ActivityIndicator,
	ScrollView,
	Animated,
	TouchableOpacity, 
} from 'react-native'
import { useFocusEffect } from '@react-navigation/native'
import { BaseStyle, useTheme, Api, BaseColor } from '@config'
import { useTranslation } from 'react-i18next'
import {
	Header,
	SafeAreaView,
	Icon,
	ListThumbCircle,
	FilterSort,
	Text,
	Button,
} from '@components'
import styles from './styles'
import { NotificationData } from '@data'
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import Modal from 'react-native-modal'
import { onChange } from 'react-native-reanimated'

export default function Notification({ navigation }) {
	const { t } = useTranslation()
	const dispatch = useDispatch()
	const { colors } = useTheme()
	const cardColor = colors.card
	const [loading, setLoading] = useState(true)
	const [load, setLoad] = useState(false)
	const [refreshing] = useState(false)
	const [notification] = useState(NotificationData)
	const profile = useSelector(state => state.prof)
	const [did, setDid] = useState()
	const [notif, setNotif] = useState([])
	const [notifArr, setNotifArr] = useState([])
	const [modeView, setModeView] = useState('block')
	const [toggleCheckBox, setToggleCheckBox] = useState(false)
	const [selectedValue, setSelectedValue] = useState('')
	const [arrselect, setArrSelect] = useState([])
	const [modalVisible, setModalVisible] = useState(false)
	const domain = useSelector(state => state.application.domain)
	const [timeoff, setTimeoff] = useState([
		{
			value: 2,
			text: 'Withdraw',
		},
		{
			value: 1,
			text: 'Approve',
		},
	])
	const onOK = () => {
		setLoading(false)
		setLoad(false)
	}

	/**
	 * call get profile
	 */
	const getProfile = () => {
		// console.log("getProfile")
		let url =
			domain +
			'/' +
			Api.apiGtkUrl +
			'/' +
			Api.pesan +
			'/' +
			profile.prof[0].iduser +
			'/3'
		axios({
			method: 'get',
			url: url,
			headers: {
				'Content-Type': 'application/json',
				Authorization: profile.prof[0].token,
			},
		})
			.then(function(resp) {
				// console.log("url : "+profile.prof[0].token)
				console.log('function get 1 --> ' + JSON.stringify(resp.data.data))
				if (resp.data.status == '1') {
					setNotif(resp.data.data)
					setNotifArr(resp.data.data)
					setDid('1')
				}
				setLoading(false)
			})
			.catch(error => {
				setLoading(false)
				console.log(
					'error api project notification = ' + JSON.stringify(error),
				)
				if (error.response) {
					Alert.alert(
						'Alert',
						error.message,
						[{ text: 'OK', onPress: () => onOK() }],
						{ cancelable: false },
					)
				} else if (error.request) {
					Alert.alert(
						'Alert',
						error.request,
						[{ text: 'OK', onPress: () => onOK() }],
						{ cancelable: false },
					)
				} else {
					Alert.alert(
						'Alert',
						error.message,
						[{ text: 'OK', onPress: () => onOK() }],
						{ cancelable: false },
					)
				}
			})
	}

	// useFocusEffect(
	// 	useCallback(() => {
	// 		let isActive = true;
	// 		console.log("notif 1")
	// 		if(isActive){
	// 	  		isActive = false
	// 			getProfile()
	// 		}
	// 	  return () => {
	// 		console.log("notif 2")
	// 		isActive = false
	// 	  };
	// 	}, [notif])
	// )

	useEffect(() => {
		getProfile()
	}, [did])

	const onRefresh = () => {
		setLoading(true)
		getProfile()
	}

	const onChange = x => {
		console.log('masuk callback : ' + x)
	}

	const onFilter = () => {
		console.log('masuk')
		navigation.navigate('Filter', { onGoBack: x => onChange(x) })
	}

	const onApprove = () => {
		console.log('onApprove')
		let count = arrselect.length
		if (count == 0) {
			Alert.alert(
				'',
				t('pilih'),
				[{ text: 'OK', onPress: () => console.log('OK Pressed') }],
				{ cancelable: false },
			)
		} else {
			setModalVisible(true)
		}
	}

	const onChangeView = () => {}

	const scrollAnim = new Animated.Value(0)
	const offsetAnim = new Animated.Value(0)
	const clampedScroll = Animated.diffClamp(
		Animated.add(
			scrollAnim.interpolate({
				inputRange: [0, 1],
				outputRange: [0, 1],
				extrapolateLeft: 'clamp',
			}),
			offsetAnim,
		),
		0,
		40,
	)
	const navbarTranslate = clampedScroll.interpolate({
		inputRange: [0, 40],
		outputRange: [0, -40],
		extrapolate: 'clamp',
	})
	const onChangeSort = isi => {
		console.log('onChangeSort ' + notif.length + ', ' + JSON.stringify(isi))
		if (notif.length == 0) {
			Alert.alert(
				'Alert',
				'Data not loaded',
				[{ text: 'OK', onPress: () => console.log }],
				{ cancelable: false },
			)
		} else {
			if (isi.value == 'low_price') {
				setLoading(true)
				setTimeout(() => {
					setNotif(notif.sort((a, b) => a.dari > b.dari))
					setLoading(false)
				}, 500)
				// console.log(notif.sort((a,b) => a.dari > b.dari))
			} else if (isi.value == 'hight_price') {
				setLoading(true)
				setTimeout(() => {
					setNotif(notif.sort((a, b) => b.dari > a.dari))
					setLoading(false)
				}, 500)
				// console.log(notif.sort((a,b) => b.dari > a.dari))
			} else if (isi.value == 'default') {
				setLoading(true)
				setTimeout(() => {
					setNotif(notifArr)
					setLoading(false)
				}, 500)
				// console.log(notif.sort((a,b) => b.dari > a.dari))
			}
		}
	}

	const renderImage = item => {
		if (item.type == 'GSIMPOK-P2K') {
			if (
				item.progres.id == '5' ||
				item.progres.id == '4' ||
				item.progres.id == '3'
			)
				return 'car-crash'
			else return 'car'
		} else {
			if (item.progres.id == '5') return 'user-times'
			else if (item.progres.id == '4') return 'user-check'
			else return 'user-clock'
		}
	}

	const renderWarna = item => {
		if (item.type == 'GSIMPOK-P2K') {
			if (
				item.progres.id == '5' ||
				item.progres.id == '4' ||
				item.progres.id == '3'
			) {
				return colors.redColor
			} else if (item.progres.id == '0') {
				return colors.yellowColor
			} else if (item.progres.id == '2') {
				return colors.greenColor
			}
			else{
				return colors.greenColor
			}
		} else {
			if (item.progres.id == '5') return colors.redColor
			else if (item.progres.id == '4') return colors.greenColor
			else colors.orangeColor
		}
	}

	const onPilih = item => {
		if (item.type == 'GSIMPOK-P2K')
			navigation.navigate('SimpokDetail', { item })
		else navigation.navigate('AttendanceDetail', { item })
	}

	const onSplit = isi => {
		let id = isi.split('|')[0]
		let type = isi.split('|')[1]
		let id_type = isi.split('|')[2]
		let isSelected = isi.split('|')[3]
		console.log(
			'onSplit : ' +
				id +
				', ' +
				type +
				', ' +
				id_type +
				', ' +
				isSelected,
		)
		toggleItem(id, type, id_type, isSelected)
	}

	const toggleItem = (itemId, type, id_types, select) => {
		let types = type.split('-')
		let value = types[1] + '=' + itemId + '/' + id_types
		console.log(
			'toggle Item --> ' +
				itemId +
				', ' +
				', ' +
				selectedValue[itemId] +
				', ' +
				select,
		)
		console.log(
			'log : ' + JSON.stringify(arrselect.filter(item => item != value)),
		)
		if (select == 'true') {
			console.log('true')
			setArrSelect(arrselect.filter(item => item != value))
		} else {
			console.log('false')
			setArrSelect([...arrselect, value])
		}
		console.log('ArrSelect : ' + JSON.stringify(arrselect))
	}

	const arrayCountPlus = int => {
		let val = arrselect.length
		let count = val + int
	}

	const arrayCountMinus = int => {
		let val = arrselect.length
		let count = val - int
	}

	const onApply = isi => {
		setModalVisible(false)
		setLoading(true)
		// let filter = timeoff.filter((item) => item.checked == true).map(item => item)
		console.log('onApply : ' + isi)
		let arr = arrselect
		let vals = arr.join('|')
		let url = domain + '/api-amsys/approval/all'

		let data = new FormData()
		data.append('nik', profile.prof[0].nik)
		data.append('data', vals)
		data.append('approval', isi)

		console.log('datanya : ' + JSON.stringify(data))
		console.log('urlnya : ' + url)
		axios({
			method: 'post',
			url: url,
			data: data,
		})
			.then(response => {
				setLoad(false)
				setLoading(false)
				console.log(' Response : ' + JSON.stringify(response.data))
				Alert.alert(
					'',
					response.data.message,
					[{ text: 'OK', onPress: () => onRefresh() }],
					{ cancelable: false },
				)
			})
			.catch(error => {
				console.log('error api project = ' + JSON.stringify(error))
				setLoad(false)
				if (error.response) {
					Alert.alert(
						'Alert',
						error.message,
						[{ text: 'OK', onPress: () => onOK() }],
						{ cancelable: false },
					)
				} else if (error.request) {
					Alert.alert(
						'Alert',
						error.request,
						[{ text: 'OK', onPress: () => onOK() }],
						{ cancelable: false },
					)
				} else {
					Alert.alert(
						'Alert',
						error.message,
						[{ text: 'OK', onPress: () => onOK() }],
						{ cancelable: false },
					)
				}
			})
	}

	const onSelectFilter = selected => {
		// console.log("selected : "+JSON.stringify(selected))
		setTimeoff(
			timeoff.map(item => {
				return {
					...item,
					checked: item.value == selected.value,
				}
			}),
		)
	}

	const renderItem = function({ item }) {
		return (
			<ListThumbCircle
				image={renderImage(item)}
				warna={renderWarna(item)}
				item={item}
				txtLeft={item.type}
				txtLeftTitle={
					item.dari == 'ATTENDANCE'
						? profile.prof[0].full_name
						: item.dari
				}
				txtContent={item.progres.ket}
				txtRight={item.waktu.split(' ')[0]}
				txtRightContent={item.waktu.split(' ')[1]}
				style={{ marginBottom: 5 }}
				checkView={
					item.dari_nik != profile.prof[0].nik &&
					item.progres.id == '0' &&
					item.dari != 'ATTENDANCE'
						? item.id
						: ''
				}
				onPress={() => {
					onPilih(item)
				}}
				onCheck={xx => {
					onSplit(xx)
				}}
			/>
			// <View style={styles.left}>
			// 	<Text bold semibold>
			// 		{item.type}
			// 	</Text>
			// 	<Text note semibold>
			// 		{item.progres.ket}
			// 	</Text>
			// 	<Text
			// 		note
			// 		// numberOfLines={1}
			// 		footnote
			// 		primaryColor
			// 		style={{
			// 			paddingTop: 5,
			// 		}}>
			// 		{item.dari}
			// 	</Text>
			// </View>
		)
	}

	const keyExtractor = useCallback(item => item.id.toString(), [])

	const getHeader = () => {
		return <Text>{'My Title'}</Text>
	}

	return (
		<SafeAreaView
			style={BaseStyle.safeAreaView}
			forceInset={{ top: 'always' }}>
			<Header
				title={t('notification')}
				// renderLeft={() => {
				// 	return (
				// 		<Icon
				// 			name="arrow-left"
				// 			size={20}
				// 			color={colors.primary}
				// 			enableRTL={true}
				// 		/>
				// 	)
				// }}
				// onPressLeft={() => {
				// 	navigation.goBack()
				// }}
			/>
			<Animated.View
				style={[
					styles.navbar,
					{ transform: [{ translateY: navbarTranslate }] },
				]}>
				<FilterSort
					// modeView={modeView}
					onChangeSort={xx => onChangeSort(xx)}
					onChangeView={onChangeView}
					onFilter={onFilter}
					onApprove={onApprove}
					labelApprove={
						profile.prof[0].level == '3' ||
						profile.prof[0].level == '4'
							? 1
							: ''
					}
					sortOption={[
						{
							value: 'default',
							icon: 'sort-amount-down',
							text: 'Default',
						},
						{
							value: 'low_price',
							icon: 'sort-amount-up',
							text: 'lowest_price',
						},
						{
							value: 'hight_price',
							icon: 'sort-amount-down',
							text: 'hightest_price',
						},
					]}
				/>
			</Animated.View>
			<Modal
				isVisible={modalVisible}
				onSwipeComplete={() => {
					setModalVisible(false)
				}}
				swipeDirection={['down']}
				style={styles.bottomModal}>
				<View
					style={[
						styles.contentFilterBottom,
						{ backgroundColor: cardColor },
					]}>
					<View style={styles.contentSwipeDown}>
						<View style={styles.lineSwipeDown} />
					</View>
					{/* {timeoff.map((item, index) => (
					<TouchableOpacity
					style={[
						styles.contentActionModalBottom,
						{borderBottomColor: colors.border},
					]}
					key={item.value}
					onPress={() => onSelectFilter(item)}>
					<Text body2 semibold primaryColor={item.checked}>
						{t(item.text)}
					</Text>
					{item.checked && (
						<Icon name="check" size={14} color={colors.primary} />
					)}
					</TouchableOpacity>
				))} */}
					<View
						style={{
							alignContent: 'center',
							alignItems: 'center',
							margin: 20,
						}}>
						<Text body1 bold>
							{'Please select an Approval'}
						</Text>
					</View>
					<View
						style={{
							flexDirection: 'row',
							justifyContent: 'space-between',
						}}>
						<Button
							loading={load}
							style={{ marginTop: 10, marginBottom: 20 }}
							onPress={() => onApply('2')}>
							{t('Withdraw')}
						</Button>
						<Button
							loading={load}
							style={{ marginTop: 10, marginBottom: 20 }}
							onPress={() => onApply('1')}>
							{t('Approve')}
						</Button>
					</View>
				</View>
			</Modal>
			{loading ? (
				<ActivityIndicator
					size="small"
					color={colors.primary}
					style={{ paddingLeft: 5 }}
				/>
			) : (
				<FlatList
					removeClippedSubviews
					// ListHeaderComponent={getHeader}
					contentContainerStyle={{
						paddingHorizontal: 20,
						paddingVertical: 10,
					}}
					refreshControl={
						<RefreshControl
							colors={[colors.primary]}
							tintColor={colors.primary}
							refreshing={refreshing}
							onRefresh={() => {
								onRefresh()
							}}
						/>
					}
					data={notif}
					keyExtractor={keyExtractor}
					extraData={selectedValue}
					renderItem={renderItem}
					initialNumToRender={5}
					maxToRenderPerBatch={10}
				/>
			)}
		</SafeAreaView>
	)
}
