import React, { useState, useEffect, useCallback } from 'react'
import {
	View,
	ScrollView,
	TouchableOpacity,
	PermissionsAndroid,FlatList,ActivityIndicator,RefreshControl
} from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { AuthActions } from '@actions'
import { BaseStyle, useTheme, Api, Images } from '@config'
import {
	Header,
	SafeAreaView,
	Icon,
	Text,
	Button,
	ProfileDetail,
	ProfilePerformance, Image,ListThumbQuran
} from '@components'
import styles from './styles'
import { useTranslation } from 'react-i18next'
import axios from 'axios'

export default function Quranread({ route, navigation }) {
	const { colors } = useTheme()
	const { t } = useTranslation()
	const [did, setDid] = useState()
	const [notif, setNotif] = useState([])
	const [notifArr, setNotifArr] = useState([])
	const [loading, setLoading] = useState(true)
	const dispatch = useDispatch()
	const [refreshing] = useState(false)
	const keyExtractor = useCallback(item => item.id.toString(), [])
	let { item } = route.params

 	const getSurah = () => {
	console.log("getSurahDetail")
	let url = Api.mainUrl + '/' + Api.surahUrl + "/" + item.number
	axios({
		method: 'get',
		url: url
	})
		.then(function(resp) {
			console.log('function get Surah Detail --> ' + JSON.stringify(resp.data.data))
			if (resp.data.status == 'OK') {
				setNotif(resp.data.data.ayahs)
				setNotifArr(resp.data.data.ayahs)
				setDid('1')
			}
			console.log("notifikasi Surah Detail: "+JSON.stringify(notif));
			setLoading(false)
		})
		.catch(error => {
			setLoading(false)
			console.log(
				'error api project notification = ' + JSON.stringify(error),
			)
			if (error.response) {
				Alert.alert(
					'Alert',
					error.message,
					[{ text: 'OK', onPress: () => onOK() }],
					{ cancelable: false },
				)
			} else if (error.request) {
				Alert.alert(
					'Alert',
					error.request,
					[{ text: 'OK', onPress: () => onOK() }],
					{ cancelable: false },
				)
			} else {
				Alert.alert(
					'Alert',
					error.message,
					[{ text: 'OK', onPress: () => onOK() }],
					{ cancelable: false },
				)
			}
		})
	}

	const onPilih = item => {
		navigation.navigate('QuranDetail', {item})
	}

	useEffect(() => {
		getSurah()
	}, [did])

	const renderItem = function({ item }) {
		return (
			<ListThumbQuran
				item={item}
				txtLeft={item.numberInSurah}
				txtRight={item.text}
				onPress={() => {
					onPilih(item)
				}}
			/>
		)
	}

	return (
		<SafeAreaView
			style={BaseStyle.safeAreaView}
			forceInset={{ top: 'always' }}>
			<Header
				title={t('read')}
				renderLeft={() => {
					return (
						<Icon
							name="arrow-left"
							size={20}
							color={colors.primary}
							enableRTL={true}
						/>
					)
				}}
				onPressLeft={() => {
					navigation.goBack()
				}}
			/>
			{loading ? (
				<ActivityIndicator
					size="small"
					color={colors.primary}
					style={{ paddingLeft: 5 }}
				/>
			) : (
				<FlatList
					removeClippedSubviews
					// ListHeaderComponent={getHeader}
					contentContainerStyle={{
						paddingHorizontal: 20,
						paddingVertical: 10,
					}}
					refreshControl={
						<RefreshControl
							colors={[colors.primary]}
							tintColor={colors.primary}
							refreshing={refreshing}
							onRefresh={() => {
								onRefresh()
							}}
						/>
					}
					data={notif}
					keyExtractor={item => item.number}
					renderItem={renderItem}
					initialNumToRender={5}
					maxToRenderPerBatch={10}
				/>
			)}
		</SafeAreaView>
	)
}
