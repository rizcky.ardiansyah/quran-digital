import React, {useState, useEffect} from 'react';
import {
  View,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Alert,
} from 'react-native';
import {BaseStyle, useTheme, Api, BaseColor} from '@config';
import {useTranslation} from 'react-i18next';
import {Header, SafeAreaView, Icon, Text, Button, TextInput} from '@components';
import styles from './styles';
import axios from 'axios';
import {useSelector, useDispatch} from 'react-redux';
import DeviceInfo from 'react-native-device-info';

export default function ChangePassword({navigation}) {
  const domain = useSelector(state => state.application.domain)
  const {t} = useTranslation();
  const [loading, setLoading] = useState(false);
  const [oldpassword, setOldPassword] = useState('');
  const [password, setPassword] = useState('');
  const [repassword, setRepassword] = useState('');
  const [success, setSuccess] = useState({
    oldpassword: true,
    repassword: true,
    password: true,
  });
  const [systemName, setSystemName] = useState();
  const [deviceId, setDeviceId] = useState();
  const [systemVersion, setSystemVersion] = useState();
  const [version, setVersion] = useState();
  const [brand, setBrand] = useState();
  const [model, setModel] = useState();
  const [devType, setDevType] = useState();
  const {colors} = useTheme();
  const profile = useSelector(state => state.prof);
  const [iconPassword, setIconPassword] = useState({icEye: "eye",showPassword: true});
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });

  const onChangePwdTtype = () => {
		console.log("onChangePwdTtype "+iconPassword.showPassword);
		if(iconPassword.showPassword == true)
			setIconPassword({icEye: "eye-slash", showPassword: false});
		else
			setIconPassword({icEye: "eye", showPassword: true});
	}

  useEffect(() => {
    setSystemName(DeviceInfo.getSystemName);
    setDevType(DeviceInfo.getDeviceType);
    setModel(DeviceInfo.getModel);
    setBrand(DeviceInfo.getBrand);
    setSystemVersion(DeviceInfo.getSystemVersion);
    setVersion(DeviceInfo.getVersion);
  }, []);

  const onOK = () => {
		setOldPassword('')
		setPassword('')
    setRepassword('')
		setLoading(false)
	}

  const onSubmit = () => {
    if (repassword === '' || password === '' || oldpassword === '') {
      setSuccess({
        ...success,
        oldpassword: false,
        repassword: false,
        password: false,
      });
    } else {
      if (repassword !== password) {
        setSuccess({
          ...success,
          repassword: false,
          password: true,
        });
        setRepassword('');
      } else {
        setLoading(true);
        let url = domain + '/' + Api.apiGtkUrl + '/user/password/' + profile.prof[0].iduser;
        let bodyFormData = {
          old: oldpassword,
          new: password
        };
        // console.log("url : "+url)
        axios({
          method: 'put',
          url: url,
          data: bodyFormData,
          headers: {
            'Content-Type': 'application/json',
            appsession: profile.prof[0].token,
            'User-Agent':
              brand + ' ' + model + ' ' + systemName + ' ' + systemVersion,
          },
        })
          .then(function(resp) {
            console.log('hasil update -> ' + JSON.stringify(resp));
            setLoading(false);
            if (resp.data.status === '1') {
              Alert.alert(
                'OK',
                resp.data.message,
                [
                  {
                    text: 'OK',
                    onPress: () => {
                      navigation.goBack();
                    },
                  },
                ],
                {cancelable: false},
              );
            } else {
              Alert.alert(
                'Alert',
                resp.data.message,
                [
                  {
                    text: 'OK',
                    onPress: () => {
                      setOldPassword('');
                      setPassword('');
                      setRepassword('');
                    },
                  },
                ],
                {cancelable: false},
              );
            }
          })
          .catch(function(error) {
            if (error.response) {
              console.log(error.response)
              Alert.alert(
                'Alert',
                error.response.data.message,
                [{ text: 'OK', onPress: () => onOK() }],
                { cancelable: false },
              )
            } else if (error.request) {
              console.log(error.request)
              Alert.alert(
                'Alert',
                error.request,
                [{ text: 'OK', onPress: () => onOK() }],
                { cancelable: false },
              )
            } else {
              console.log(error)
              Alert.alert(
                'Alert',
                error.message,
                [{ text: 'OK', onPress: () => onOK() }],
                { cancelable: false },
              )
            }
            // setLoading(false);
          });
      }
    }
  };

  return (
    <SafeAreaView style={BaseStyle.safeAreaView} forceInset={{top: 'always'}}>
      <Header
        title={t('change_password')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'android' ? 'height' : 'padding'}
        keyboardVerticalOffset={offsetKeyboard}
        style={{flex: 1, justifyContent: 'center'}}>
        <ScrollView
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'center',
            padding: 20,
          }}>
          <View style={styles.contentTitle}>
            <Text headline semibold>
              {t('old_password')}
            </Text>
          </View>
          <TextInput
            onChangeText={text => setOldPassword(text)}
            secureTextEntry={iconPassword.showPassword}
            placeholder="Old Password"
            value={oldpassword}
            onFocus={() => {
              setSuccess({
                ...success,
                oldpassword: true,
              });
            }}
            icon={<Icon
							name={iconPassword.icEye}
							size={14}
							solid
							color={BaseColor.grayColor}
							onPress={()=>onChangePwdTtype()}
						/>}
            success={success.oldpassword}
          />
          <View style={styles.contentTitle}>
            <Text headline semibold>
              {t('password')}
            </Text>
          </View>
          <TextInput
            onChangeText={text => setPassword(text)}
            secureTextEntry={iconPassword.showPassword}
            placeholder="Password"
            value={password}
            onFocus={() => {
              setSuccess({
                ...success,
                password: true,
              });
            }}
            icon={<Icon
							name={iconPassword.icEye}
							size={14}
							solid
							color={BaseColor.grayColor}
							onPress={()=>onChangePwdTtype()}
						/>}
            success={success.password}
          />
          <View style={styles.contentTitle}>
            <Text headline semibold>
              {t('re_password')}
            </Text>
          </View>
          <TextInput
            onChangeText={text => setRepassword(text)}
            secureTextEntry={iconPassword.showPassword}
            success={success.repassword}
            placeholder={t('password_confirm')}
            value={repassword}
            onFocus={() => {
              setSuccess({
                ...success,
                repassword: true,
              });
            }}
            icon={<Icon
							name={iconPassword.icEye}
							size={14}
							solid
							color={BaseColor.grayColor}
							onPress={()=>onChangePwdTtype()}
						/>}
          />
          <View style={{paddingVertical: 15}}>
            <Button
              loading={loading}
              full
              onPress={() => {
                onSubmit();
              }}>
              {t('confirm')}
            </Button>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
