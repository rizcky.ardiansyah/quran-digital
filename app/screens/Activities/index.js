import React, { useState, useEffect } from 'react'
import {
	RefreshControl,
	FlatList,
	Alert,
	ActivityIndicator,
	View,
	Text,
	Button,
	Animated,
} from 'react-native'
import { BaseStyle, useTheme, Api } from '@config'
import { useTranslation } from 'react-i18next'
import {
	Header,
	SafeAreaView,
	Icon,
	ListThumbCircle,
	FilterSort,
} from '@components'
import styles from './styles'
import axios from 'axios'
import { useSelector, useDispatch } from 'react-redux'
import Modal from 'react-native-modal'
import { BaseColor } from '@config'

export default function Notification({ navigation }) {
	const { t } = useTranslation()
	const { colors } = useTheme()

	const [refreshing] = useState(false)
	const [activities, setActivities] = useState()
	const profile = useSelector(state => state.prof)
	const [hasError, setErrors] = useState(false)
	const [loading, setLoading] = useState(false)
	const [startDate, setStartDate] = useState('')
	const [endDate, setEndDate] = useState('')
	const [MaxDate, setMaxDate] = useState(new Date())
	const [MinDate, setMinDate] = useState()
	const [selectDate, setSelectDate] = useState()
	const [calenderShow, setCalenderShow] = useState(false)
	const [filter, setFilter] = useState(false)
	const [notif, setNotif] = useState([])
	const [notifArr, setNotifArr] = useState([])
	/**
	 * call when action login
	 *
	 */
	const onLogin = () => {
		// console.log('masuk ac')
		// return
		setLoading(true)
		var past = new Date()
		var day = past.getDay()
		past.setDate(past.getDate() - day)
		var pastDay = past.toISOString()
		var pastDaySTR = pastDay.slice(0, 10)
		var curDate = new Date()
		var now = curDate.toISOString()
		var nowSTR = now.slice(0, 10)
		console.log('ini tanggal', pastDaySTR, nowSTR, day)
		let url =
			Api.gitaMainUrl +
			'/' +
			Api.userInfoUrl +
			'/' +
			profile.gita.id +
			'/' +
			pastDaySTR +
			'/' +
			nowSTR
		console.log('ini url', url)
		axios({
			method: 'get',
			url: url,
			headers: {
				'Content-Type': 'multipart/form-data',
				appsession: profile.gita.token,
			},
		})
			.then(function(resp) {
				// console.log('hasil activate -> ' + JSON.stringify(resp))
				if (resp.data.status === '1') {
					setActivities(resp.data.data)
					setNotif(resp.data.data)
					setNotifArr(resp.data.data)
				} else {
					setErrors(true)
					Alert.alert(
						'Alert',
						resp.data.message,
						[{ text: 'OK', onPress: () => navigation.goBack() }],
						{ cancelable: false },
					)
				}
				setLoading(false)
			})
			.catch(function(response) {
				console.log('ini error', response)
				setLoading(false)
			})
	}

	const scrollAnim = new Animated.Value(0)
	const offsetAnim = new Animated.Value(0)
	const clampedScroll = Animated.diffClamp(
		Animated.add(
			scrollAnim.interpolate({
				inputRange: [0, 1],
				outputRange: [0, 1],
				extrapolateLeft: 'clamp',
			}),
			offsetAnim,
		),
		0,
		40,
	)
	const navbarTranslate = clampedScroll.interpolate({
		inputRange: [0, 40],
		outputRange: [0, -40],
		extrapolate: 'clamp',
	})

	const showDatePicker = () => {
		setCalenderShow(true)
		console.log(selectDate)
	}
	const hideDatePicker = () => {
		setCalenderShow(false)
	}
	const onSelect = date => {
		if (selectDate == 2 && startDate == '') {
			alert('select start date first')
		} else {
			setMinDate(date)
			var dateStr = date.toISOString()
			var startDate = dateStr.slice(0, 10)
			if (selectDate == 1) {
				setStartDate(startDate)
			} else {
				setEndDate(startDate)
			}
		}

		hideDatePicker()
	}
	const onApply = (start, end) => {
		console.log('masuk', start, end)
		if (start == '' || end == '') {
			alert('Start and End Date must be select')
		} else {
			setMinDate()
			let url =
				Api.gitaMainUrl +
				'/' +
				Api.userInfoUrl +
				'/' +
				profile.gita.id +
				'/' +
				start +
				'/' +
				end
			console.log('ini url', url)
			axios({
				method: 'get',
				url: url,
				headers: {
					'Content-Type': 'multipart/form-data',
					appsession: profile.gita.token,
				},
			})
				.then(function(resp) {
					// console.log('hasil activate -> ' + JSON.stringify(resp))
					if (resp.data.status === '1') {
						console.log('masuk activity')
						setActivities(resp.data.data)
					} else {
						setErrors(true)
						Alert.alert(
							'Alert',
							resp.data.message,
							[
								{
									text: 'OK',
									onPress: () => navigation.goBack(),
								},
							],
							{ cancelable: false },
						)
					}
					setLoading(false)
				})
				.catch(function(response) {
					console.log('ini error', response)
					setLoading(false)
				})
		}
	}

	const onChange = x => {
		console.log('callback : ' + x)
		var start = x.substring(0, 10)
		var end = x.substring(11, 21)
		onApply(start, end)
	}
	const onFilter = () => {
		console.log('masuk')
		navigation.navigate('Filter', { onGoBack: x => onChange(x) })
	}

	const onApprove = () => {
		console.log('onApprove')
		let count = arrselect.length
		if (count == 0) {
			Alert.alert(
				'',
				t('pilih'),
				[{ text: 'OK', onPress: () => console.log('OK Pressed') }],
				{ cancelable: false },
			)
		} else {
			setModalVisible(true)
		}
	}

	const onChangeSort = isi => {
		console.log('onChangeSort ' + notif.length + ', ' + JSON.stringify(isi))
		if (notif.length == 0) {
			Alert.alert(
				'Alert',
				'Data not loaded',
				[{ text: 'OK', onPress: () => console.log }],
				{ cancelable: false },
			)
		} else {
			if (isi.value == 'low_price') {
				setLoading(true)
				setTimeout(() => {
					setNotif(notif.sort((a, b) => a.dari > b.dari))
					setLoading(false)
				}, 500)
				// console.log(notif.sort((a,b) => a.dari > b.dari))
			} else if (isi.value == 'hight_price') {
				setLoading(true)
				setTimeout(() => {
					setNotif(notif.sort((a, b) => b.dari > a.dari))
					setLoading(false)
				}, 500)
				// console.log(notif.sort((a,b) => b.dari > a.dari))
			} else if (isi.value == 'default') {
				setLoading(true)
				setTimeout(() => {
					setNotif(notifArr)
					setLoading(false)
				}, 500)
				// console.log(notif.sort((a,b) => b.dari > a.dari))
			}
		}
	}

	const onSplit = isi => {
		let id = isi.split('|')[0]
		let type = isi.split('|')[1]
		let id_type = isi.split('|')[2]
		let isSelected = isi.split('|')[3]
		console.log(
			'onSplit : ' +
				id +
				', ' +
				type +
				', ' +
				id_type +
				', ' +
				isSelected,
		)
		toggleItem(id, type, id_type, isSelected)
	}

	const toggleItem = (itemId, type, id_types, select) => {
		let types = type.split('-')
		let value = types[1] + '=' + itemId + '/' + id_types
		console.log(
			'toggle Item --> ' +
				itemId +
				', ' +
				', ' +
				selectedValue[itemId] +
				', ' +
				select,
		)
		console.log(
			'log : ' + JSON.stringify(arrselect.filter(item => item != value)),
		)
		if (select == 'true') {
			console.log('true')
			setArrSelect(arrselect.filter(item => item != value))
		} else {
			console.log('false')
			setArrSelect([...arrselect, value])
		}
		console.log('ArrSelect : ' + JSON.stringify(arrselect))
	}
	const onChangeView = () => {}

	useEffect(() => {
		onLogin()
		console.log('ini acti')
	}, [])

	// console.log('activities --> ' + JSON.stringify(activities));

	return (
		<SafeAreaView
			style={BaseStyle.safeAreaView}
			forceInset={{ top: 'always' }}>
			<Header
				title={t('history')}
				renderLeft={() => {
					return (
						<Icon
							name="arrow-left"
							size={20}
							color={colors.primary}
							enableRTL={true}
						/>
					)
				}}
				onPressLeft={() => {
					navigation.goBack()
				}}
			/>
			<Animated.View
				style={[
					styles.navbar,
					{ transform: [{ translateY: navbarTranslate }] },
				]}>
				<FilterSort
					// modeView={modeView}
					onChangeSort={xx => onChangeSort(xx)}
					onChangeView={onChangeView}
					onFilter={onFilter}
					onApprove={onApprove}
					sortOption={[
						{
							value: 'default',
							icon: 'sort-amount-down',
							text: 'Default',
						},
						{
							value: 'low_price',
							icon: 'sort-amount-up',
							text: 'lowest_price',
						},
						{
							value: 'hight_price',
							icon: 'sort-amount-down',
							text: 'hightest_price',
						},
					]}
				/>
			</Animated.View>
			{loading ? (
				<ActivityIndicator
					size="small"
					color={colors.primary}
					style={{ paddingLeft: 5 }}
				/>
			) : (
				<FlatList
					contentContainerStyle={{
						paddingHorizontal: 20,
						paddingVertical: 10,
					}}
					refreshControl={
						<RefreshControl
							colors={[colors.primary]}
							tintColor={colors.primary}
							refreshing={refreshing}
							onRefresh={() => {}}
						/>
					}
					data={activities}
					keyExtractor={(item, index) => item.id}
					renderItem={({ item, index }) => (
						<ListThumbCircle
							txtLeftTitle={item.action}
							txtContent={item.info}
							txtRight={item.date}
							txtRightContent={item.time}
							style={{ marginBottom: 5 }}
						/>
					)}
				/>
			)}
		</SafeAreaView>
	)
}
