import React, { useState, useEffect, useCallback } from 'react'
import {
	RefreshControl,
	FlatList,
	ActivityIndicator,
	ScrollView,
	Animated,
	Alert,
	View,
} from 'react-native'
import { useFocusEffect } from '@react-navigation/native'
import { BaseStyle, useTheme, Api, BaseColor } from '@config'
import { useTranslation } from 'react-i18next'
import {
	Header,
	SafeAreaView,
	Icon,
	ListThumbCircle,
	FilterSort,
	Text,
} from '@components'
import styles from './styles'
import { NotificationData } from '@data'
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import moment from 'moment'
import { Button } from 'react-native'

export default function Penilaian({ navigation }) {
	const domain = useSelector(state => state.application.domain)
	const { t } = useTranslation()
	const dispatch = useDispatch()
	const { colors } = useTheme()
	const [loading, setLoading] = useState(true)
	const [refreshing] = useState(false)
	const [notification] = useState(NotificationData)
	const profile = useSelector(state => state.prof)
	const [notif, setNotif] = useState()
	const [notifArr, setNotifArr] = useState()
	const [modeView, setModeView] = useState('block')
	const [toggleCheckBox, setToggleCheckBox] = useState(false)
	const [isEnable, setIsEnable] = useState(false)

	// useFocusEffect(
	// 	useCallback(() => {
	// 	  getProfile()
	// 	  return () => {
	// 		getProfile()
	// 	  };
	// 	}, [])
	// )

	const onOK = () => {
		setLoading(false)
	}

	/**
	 * call get profile
	 */
	const getProfile = () => {
		console.log('ini profil', profile.prof[0].nik)
		let url
		setLoading(true)
		if (profile.prof[0].level == 6) {
			url =
				domain +
				'/apipenilaian/history/penilaian/' +
				profile.prof[0].iduser
		} else if (profile.prof[0].level == 3 || profile.prof[0].level == 4) {
			url =
				domain + '/apipenilaian/kinerja/getstaff/' + profile.prof[0].nik
		}

		console.log('ini domain', domain)
		axios({
			method: 'get',
			url: url,
			headers: {
				'Content-Type': 'application/json',
				appsession: profile.prof[0].token,
			},
		})
			.then(function(resp) {
				console.log(
					'ngambil history --> ' + JSON.stringify(resp.data.data),
				)
				setLoading(false)
				if (resp.data.status == '1') {
					console.log('masuk kok')
					setNotif(resp.data.data)
					setNotifArr(resp.data.data)
					setIsEnable(true)
				} else {
				}
			})
			.catch(error => {
				console.log('error api project = ' + JSON.stringify(error))
				if (error.response) {
					Alert.alert(
						'Alert',
						error.message,
						[{ text: 'OK', onPress: () => onOK() }],
						{ cancelable: false },
					)
				} else if (error.request) {
					Alert.alert(
						'Alert',
						error.request,
						[{ text: 'OK', onPress: () => onOK() }],
						{ cancelable: false },
					)
				} else {
					Alert.alert(
						'Alert',
						error.message,
						[{ text: 'OK', onPress: () => onOK() }],
						{ cancelable: false },
					)
				}
			})
	}

	useEffect(() => {
		getProfile()
	}, [isEnable])

	const onRefresh = () => {
		getProfile()
	}

	const onChange = x => {
		console.log('callback : ' + x)
	}

	const onFilter = () => {
		navigation.navigate('Filter', { onGoBack: x => onChange(x) })
	}

	const onApprove = () => {
		console.log('onApprove')
	}

	const onChangeView = () => {}

	const scrollAnim = new Animated.Value(0)
	const offsetAnim = new Animated.Value(0)
	const clampedScroll = Animated.diffClamp(
		Animated.add(
			scrollAnim.interpolate({
				inputRange: [0, 1],
				outputRange: [0, 1],
				extrapolateLeft: 'clamp',
			}),
			offsetAnim,
		),
		0,
		40,
	)
	const navbarTranslate = clampedScroll.interpolate({
		inputRange: [0, 40],
		outputRange: [0, -40],
		extrapolate: 'clamp',
	})

	const onChangeSort = isi => {
		if (notif.length == 0) {
			Alert.alert(
				'Alert',
				'Data not loaded',
				[{ text: 'OK', onPress: () => console.log }],
				{ cancelable: false },
			)
		} else {
			if (isi.value == 'low_price') {
				setLoading(true)
				setTimeout(() => {
					setNotif(notif.sort((a, b) => a.dari > b.dari))
					setLoading(false)
				}, 500)
				// console.log(notif.sort((a,b) => a.dari > b.dari))
			} else if (isi.value == 'hight_price') {
				setLoading(true)
				setTimeout(() => {
					setNotif(notif.sort((a, b) => b.dari > a.dari))
					setLoading(false)
				}, 500)
				// console.log(notif.sort((a,b) => b.dari > a.dari))
			}
		}
	}

	const renderItem = function({ item }) {
		return (
			<ListThumbCircle
				image={'clipboard-list'}
				warna={
					item.status_progres === 0
						? BaseColor.orangeColor
						: item.id_progress === ''
						? BaseColor.redColor
						: BaseColor.greenColor
				}
				txtLeft={
					profile.prof[0].level == 6
						? item.detail_pegawai_kinerja.nik
						: profile.prof[0].level == 3 ||
						  profile.prof[0].level == 4
						? item.nik
						: null
				}
				txtLeftTitle={
					profile.prof[0].level == 6
						? item.detail_pegawai_kinerja.full_name
						: profile.prof[0].level == 3 ||
						  profile.prof[0].level == 4
						? item.full_name
						: null
				}
				txtContent={
					profile.prof[0].level == 6 && item.status_progres != 0
						? item.status_progress_detail.description
						: profile.prof[0].level == 6 && item.status_progres == 0
						? 'Penilaian telah dibuat'
						: profile.prof[0].level == 6 &&
						  item.status_progres == null
						? 'Penilaian belum dibuat'
						: (profile.prof[0].level == 3 &&
								item.status_progres != 0) ||
						  (profile.prof[0].level == 4 &&
								item.status_progres != 0)
						? item.status_progres.description
						: (profile.prof[0].level == 3 &&
								item.status_progres === '') ||
						  (profile.prof[0].level == 4 &&
								item.status_progres === '')
						? 'Penilaian belum dibuat'
						: (profile.prof[0].level == 3 &&
								item.status_progres === 0) ||
						  (profile.prof[0].level == 4 &&
								item.status_progres === 0)
						? 'Penilaian telah dibuat'
						: null
				}
				// txtRight={item.date_input.split(' ')[0]}
				// txtRightContent={item.date_input.split(' ')[1]}
				txtRightStatus={
					profile.prof[0].level == 6
						? item.status_progress_detail.status
						: null
				}
				style={{ marginBottom: 5 }}
				checkView={
					item.id_progress == '0' && item.dari != 'ATTENDANCE'
						? item.id
						: ''
				}
				onPress={() => navigation.navigate('PenilaianDetail', { item })}
			/>
		)
	}

	return (
		<SafeAreaView
			style={BaseStyle.safeAreaView}
			forceInset={{ top: 'always' }}>
			{profile.prof[0].level == 3 || profile.prof[0].level == 4 ? (
				<Header
					title={t('List Penilaian')}
					renderLeft={() => {
						return (
							<Icon
								name="arrow-left"
								size={20}
								color={colors.primary}
								enableRTL={true}
							/>
						)
					}}
					onPressLeft={() => {
						navigation.goBack()
					}}
					renderRight={() => {
						return (
							<Icon
								name="plus"
								size={20}
								color={colors.primary}
								enableRTL={true}
							/>
						)
					}}
					onPressRight={() => {
						navigation.navigate('PenilaianBuat')
					}}
				/>
			) : (
				<Header
					title={t('List Penilaian')}
					renderLeft={() => {
						return (
							<Icon
								name="arrow-left"
								size={20}
								color={colors.primary}
								enableRTL={true}
							/>
						)
					}}
					onPressLeft={() => {
						navigation.goBack()
					}}
				/>
			)}

			<Animated.View
				style={[
					styles.navbar,
					{ transform: [{ translateY: navbarTranslate }] },
				]}>
				<FilterSort
					// modeView={modeView}
					onChangeSort={xx => onChangeSort(xx)}
					onChangeView={onChangeView}
					onFilter={onFilter}
					sortOption={[
						{
							value: 'low_price',
							icon: 'sort-amount-up',
							text: 'lowest_price',
						},
						{
							value: 'hight_price',
							icon: 'sort-amount-down',
							text: 'hightest_price',
						},
					]}
				/>
			</Animated.View>

			{loading ? (
				<ActivityIndicator
					size="small"
					color={colors.primary}
					style={{ paddingLeft: 5 }}
				/>
			) : (
				<View>
					<FlatList
						contentContainerStyle={{
							paddingHorizontal: 20,
							paddingVertical: 10,
						}}
						refreshControl={
							<RefreshControl
								colors={[colors.primary]}
								tintColor={colors.primary}
								refreshing={refreshing}
								onRefresh={() => {
									onRefresh()
								}}
							/>
						}
						data={notif}
						keyExtractor={(item, index) => item.id_logbook}
						renderItem={renderItem}
					/>
				</View>
			)}
		</SafeAreaView>
	)
}
