import React, {useState} from 'react';
import {View, KeyboardAvoidingView, Platform, Alert} from 'react-native';
import { BaseStyle, useTheme, Api, BaseColor } from '@config'
import {Header, SafeAreaView, Icon, Button, TextInput} from '@components';
import styles from './styles';
import {useTranslation} from 'react-i18next';
import axios from 'axios';

export default function SignUp({navigation}) {
  const {colors} = useTheme();
  const {t} = useTranslation();
  const [iconPassword, setIconPassword] = useState({icEye: "eye",showPassword: true});
  const [username, setUsername] = useState('');
  const [name, setName] = useState('');
  const [lastname, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [phone, setPhone] = useState('');
  const [loading, setLoading] = useState(false);
  const domain = useSelector(state => state.application.domain)
  const [success, setSuccess] = useState({
    name: true,
    lastname: true,
    email: true,
    address: true,
    phone: true,
    username: true
  });

  const onChangePwdTtype = () => {
		console.log("onChangePwdTtype "+iconPassword.showPassword);
		if(iconPassword.showPassword == true)
			setIconPassword({icEye: "eye-slash", showPassword: false});
		else
			setIconPassword({icEye: "eye", showPassword: true});
	}

  const onSetNull = () => {
    setEmail('')
    setName('')
    setLastName('')
    setPassword('')
    setPhone('')
  }

  /**
   * call when action signup
   *
   */
  const onSignUp = () => {
    if (
      name === '' ||
      email === '' ||
      lastname === '' ||
      phone === '' ||
      password === ''
    ) {
      setSuccess({
        ...success,
        name: name !== '' ? true : false,
        email: email !== '' ? true : false,
        lastname: lastname !== '' ? true : false,
        phone: phone !== '' ? true : false,
        password: password !== '' ? true : false,
      });
    } else {
      setLoading(true);
      // setTimeout(() => {
      //   setLoading(false);
      //   navigation.navigate('SignIn');
      // }, 500);
      let url = domain + '/' + Api.authUrl + '/' + Api.regUrl;
      let bodyFormData = new FormData();
      bodyFormData.append('namaDepan', name);
      bodyFormData.append('namaBelakang', lastname);
      bodyFormData.append('noTelp', phone);
      bodyFormData.append('email', email);
      bodyFormData.append('username', username)
      bodyFormData.append('password', password);
      bodyFormData.append('ktp', '');
      bodyFormData.append('pathImage', '');
      bodyFormData.append('kelamin', '');
      axios({
        method: 'post',
        url: url,
        data: bodyFormData,
        headers: {'Content-Type': 'multipart/form-data'},
      })
        .then(function(resp) {
          console.log('hasil registasi -> ' + JSON.stringify(resp));
          if (resp.data.status === '1') {
            Alert.alert(
              'Alert',
              resp.data.message_ind +
                ' \n Silakan gunakan username ' +
                resp.data.data.username,
              [{text: 'OK', onPress: () => navigation.navigate('SignIn')}],
              {cancelable: false},
            );
          } else {
            Alert.alert(
              'Alert',
              resp.data.message_ind,
              [{text: 'OK', onPress: () => navigation.navigate('SignIn')}],
              {cancelable: false},
            );
          }
          setLoading(false);
        })
        .catch(function(response) {
          console.log('hasil registasi -> ' + JSON.stringify(response));
          Alert.alert(
            'Alert',
            'Email Anda Sudah Digunakan',
            [{text: 'OK', onPress: () => {onSetNull}}],
            {cancelable: false},
          );
          setLoading(false);
        });
    }
  };

  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  return (
    <SafeAreaView style={BaseStyle.safeAreaView} forceInset={{top: 'always'}}>
      <Header
        title={t('sign_up')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'android' ? 'height' : 'padding'}
        keyboardVerticalOffset={offsetKeyboard}
        style={{flex: 1}}>
        <View style={styles.contain}>
          {/* <TextInput
            onChangeText={text => setKtp(text)}
            placeholder={t('input_id')}
            success={success.ktp}
            value={ktp}
            keyboardType="numeric"
            maxLength={13}
            minLength={10}
          /> */}
          <TextInput
            style={{marginTop: 10}}
            onChangeText={text => setName(text)}
            placeholder={t('input_name')}
            success={success.name}
            value={name}
          />
          <TextInput
            style={{marginTop: 10}}
            onChangeText={text => setLastName(text)}
            placeholder={t('input_lname')}
            success={success.lastname}
            value={lastname}
          />
          <TextInput
            style={{marginTop: 10}}
            onChangeText={text => setEmail(text)}
            placeholder={t('input_email')}
            keyboardType="email-address"
            success={success.email}
            value={email}
          />
          <TextInput
            style={{marginTop: 10}}
            onChangeText={text => setPassword(text)}
            placeholder={t('input_password')}
            success={success.password}
            secureTextEntry={iconPassword.showPassword}
            value={password}
            icon={<Icon
							name={iconPassword.icEye}
							size={14}
							solid
							color={BaseColor.grayColor}
							onPress={()=>onChangePwdTtype()}/>}
          />
          <TextInput
            style={{marginTop: 10}}
            onChangeText={text => setPhone(text)}
            placeholder={t('phone_number')}
            success={success.phone}
            value={phone}
            keyboardType="numeric"
            maxLength={13}
            minLength={10}
          />
          <Button
            full
            style={{marginTop: 20}}
            loading={loading}
            onPress={() => onSignUp()}>
            {t('sign_up')}
          </Button>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
