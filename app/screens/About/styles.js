import { StyleSheet } from 'react-native'
import * as Utils from '@utils'

export default StyleSheet.create({
	imageBackground: {
		height: 100,
		width: '100%',
		position: 'absolute',
	},
	searchForm: {
		padding: 1,
		borderRadius: 10,
		borderWidth: 0.5,
		width: '100%',
		shadowColor: 'black',
		shadowOffset: { width: 1.5, height: 1.5 },
		shadowOpacity: 0.3,
		shadowRadius: 1,
		elevation: 1,
	},
	contentServiceIcon: {
		marginTop: 10,
		flexDirection: 'row',
		justifyContent: 'space-around',
	},
	contentCartPromotion: {
		marginTop: 10,
		flexDirection: 'row',
		// justifyContent: 'flex-start',
		justifyContent: 'space-between',
		alignSelf: 'flex-end'
		// justifyContent: 'flex-end'
	},
	btnPromotion: {
		height: 25,
		borderRadius: 3,
		// paddingHorizontal: 10,
		paddingVertical: 5,
	},
	contentHiking: {
		marginTop: 20,
		marginLeft: 20,
		marginBottom: 10,
	},
	promotionBanner: {
		height: Utils.scaleWithPixel(100),
		width: '100%',
		marginTop: 10,
	},
	line: {
		height: 1,
		marginTop: 10,
		marginBottom: 15,
	},
	iconContent: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 36,
		height: 36,
		borderRadius: 18,
	},
	itemService: {
		alignItems: 'center',
		justifyContent: 'center',
		flex: 1,
		paddingTop: 10,
	},
	promotionItem: {
		width: Utils.scaleWithPixel(155),
		height: Utils.scaleWithPixel(100),
	},
	tourItem: {
		width: Utils.scaleWithPixel(120),
		height: Utils.scaleWithPixel(60),
	},
	carouselItem: {
		width: Utils.scaleWithPixel(200),
		height: Utils.scaleWithPixel(250),
	},
	titleView: {
		paddingHorizontal: 20,
		paddingTop: 10,
		paddingBottom: 5,
	},
	isiContent: {
		justifyContent: 'center',
		alignItems: 'center',
		// width: '100%',
		paddingTop: 5,
		paddingLeft: 8,
	},
	profileItem: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingVertical: 20,
	},
	triangleCorner: {
		width: 0,
		height: 0,
		backgroundColor: "transparent",
		borderStyle: "solid",
		borderRightWidth: 70,
		borderTopWidth: 70,
		borderRightColor: "transparent",
		borderTopColor: "red",
		transform: [{ rotate: "90deg" }],
		alignSelf: 'flex-end',
		marginTop: -200,
		right: -79,
		position: 'absolute'
	},
})
