import React, { useState, useEffect } from 'react'
import {
	View,
	ScrollView,
	Animated,
	ActivityIndicator,
	Dimensions
} from 'react-native'
import {
	Image,
	Text,
	Icon,
	Card,
	CardIsi,
	CardIsiUn,
	Button,
	SafeAreaView,
	EventCard,
	ProfileDetailHome,
	Header
} from '@components'
import { ProfileActions } from '@actions'
import { useSelector, useDispatch } from 'react-redux'
import { BaseStyle, Images, useTheme, Api } from '@config'
import * as Utils from '@utils'
import styles from './styles'
import { PromotionData, TourData, UserData } from '@data'
import { useTranslation } from 'react-i18next'
import VersionNumber from 'react-native-version-number';

var screen = Dimensions.get('window')

export default function About({ navigation }) {
	const { t } = useTranslation()
	const dispatch = useDispatch()
	const { colors } = useTheme()
	const [userData] = useState(UserData[0])
	const [promotion, setPromotion] = useState('');
	const [images, setImages] = useState('');
	const [loading, setLoading] = useState(false)
	const [tours] = useState(TourData)
	const [heightHeader, setHeightHeader] = useState(Utils.heightHeader())
	const deltaY = new Animated.Value(0)
	const [icons, seticons] = useState('')
	const [app, setApp] = useState('')
	const [txt] = useState('Gina merupakan Aplikasi Internal berbasis Android khusus untuk karyawan Efisiensi dan kemudahan menjadi kunci utama yang menjadi landasan utama dalam setiap aktifitas karyawan dan memadukan kebutuhan dan kemampuan dalam mobilitas karyawan yang fleksibel. Untuk saat ini fitur utama yang sedang berjalan adalah G-Simpok, Attendance, dan Logbook')
	const heightImageBanner = Utils.scaleWithPixel(80)
	const marginTopBanner = heightImageBanner - heightHeader
	const profile = useSelector(state => state.prof)
	const onOK = () => {
		setLoading(false)
	}

	useEffect(() => {
		// getAplikasi()
	}, [])

	return (
			<SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always' }}>
				<Header
					title={t('about_me')}
					renderLeft={() => {
						return (
							<Icon
								name="list"
								size={20}
								color={colors.primary}
								enableRTL={true}
							/>
						)
					}}
					onPressLeft={() => {
						navigation.openDrawer()
					}}
				/>
				{loading ? (
					<ActivityIndicator
						size="small"
						color={colors.primary}
						style={{ paddingLeft: 5 }}
					/>
				) : (
					<ScrollView
						onScroll={Animated.event([
							{
								nativeEvent: {
									contentOffset: { y: deltaY },
								},
							},
						],{ useNativeDriver: false },)}
						onContentSizeChange={() =>
							setHeightHeader(Utils.heightHeader())
						}
						scrollEventThrottle={8}>
						<View style = {{ backgroundColor : '#EDEDED', width : screen.width, height : screen.height, paddingTop : 20  }}>
							<View style = {{ width : screen.width, height : 240, justifyContent : 'center', alignItems : 'center',}}>
							<Image 
								resizeMode='cover'
								style={{
									width : 200, 
									height : 200,
									borderRadius : 10,
									alignSelf: 'center',
									borderWidth : 2,
									borderColor : '#FFFFFF'
								}}
								source= {Images.logo}
							/>
							<View style = {{width : 200, height : 25, justifyContent : 'center', alignItems : 'center',}} >
								<Text style = {{ fontWeight : 'bold', fontSize : 13 }}> VERSION {VersionNumber.appVersion}</Text>
							</View>
							</View>
							<View style = {{ width : screen.width, justifyContent : 'center', alignItems : 'center', }}>
							<View style = {{ width : screen.width*0.9, paddingVertical : 10, borderTopWidth : 1, borderTopColor : '#9C9C9C'   }}>
								<Text style = {{ fontSize : 13, fontStyle : 'italic', textAlign : 'center' }}>{ txt }</Text>
							</View>
							</View>
						</View>
					</ScrollView>
				)}
			</SafeAreaView>
		// </View>
	)
}
