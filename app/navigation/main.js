import React, { useEffect, useState } from 'react'
import { StatusBar } from 'react-native'
import { Text, TouchableOpacity, Image, View } from 'react-native'
import { AnimatedTabBarNavigator } from 'react-native-animated-nav-tab-bar'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import styled from 'styled-components/native'
import { Button, Icon } from '@components'
// import { Button } from 'react-native-elements'
import { DarkModeProvider, useDarkMode } from 'react-native-dark-mode'
import { BaseColor, BaseStyle, Images, useTheme, useFont, Api } from '@config'
import SplashScreen from 'react-native-splash-screen'
import i18n from 'i18next'
import { initReactI18next, useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'

// Stack Screen
import Walkthrough from '@screens/Walkthrough'
import About from '@screens/About'
import Utama from '@screens/Utama'
import Quran from '@screens/Quran'
import axios from 'axios'

const Tabs = AnimatedTabBarNavigator()
const Tabss = createBottomTabNavigator()
const Drawer = createDrawerNavigator()
const INITIAL_ROUTE_NAME = 'Setting'
const Screen = styled.View`
	flex: 1;
	justify-content: center;
	align-items: center;
	background-color: #f2f2f2;
`

const Stack = createStackNavigator()

export default () => {
	const auth = useSelector(state => state.auth)
	const login = auth.login.success
	const { colors } = useTheme()
	const profile = useSelector(state => state.prof)

	return (
		<Stack.Navigator headerMode="none">
		
				<Stack.Screen
					name="Walkthrough"
					component={Utama}
					options={{
						title: 'Walkthrough',
						drawerIcon: ({ focused, size }) => (
							<Icon
								color={colors.primary}
								name="home"
								size={18}
								solid
							/>
						),
					}}
				/>

			<Stack.Screen name="About" component={About} />
		</Stack.Navigator>
	)
}
