import React, { useEffect } from 'react'
import { StatusBar } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { DarkModeProvider, useDarkMode } from 'react-native-dark-mode'
import { useTheme, BaseSetting } from '@config'
import SplashScreen from 'react-native-splash-screen'
import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import { useSelector } from 'react-redux'
/* Main Stack Navigator */
import Main from './main'
/* Modal Screen only affect iOS */
import Loading from '@screens/Loading'
import SelectDarkOption from '@screens/SelectDarkOption'
import SelectFontOption from '@screens/SelectFontOption'
import Setting from '@screens/Setting'
import ThemeSetting from '@screens/ThemeSetting'
import Quran from '@screens/Quran'
import QuranRead from '@screens/QuranRead'
import QuranDetail from '@screens/QuranDetail'

const Stack = createStackNavigator()

export default () => {
	const storeLanguage = useSelector(state => state.application.language)
	const { theme, colors } = useTheme()
	const isDarkMode = useDarkMode()
	const forFade = ({ current, closing }) => ({
		cardStyle: {
			opacity: current.progress,
		},
	})

	useEffect(() => {
		console.log('useEffect')
		i18n.use(initReactI18next).init({
			resources: BaseSetting.resourcesLanguage,
			lng: storeLanguage ?? BaseSetting.defaultLanguage,
			fallbackLng: BaseSetting.defaultLanguage,
		})
		SplashScreen.hide()
		StatusBar.setBackgroundColor(colors.primary, true)
		StatusBar.setBarStyle(
			isDarkMode ? 'light-content' : 'dark-content',
			true,
		)
	}, [])
	return (
		<DarkModeProvider>
			<NavigationContainer theme={theme}>
				<Stack.Navigator
					mode="modal"
					headerMode="none"
					initialRouteName="Loading">
					<Stack.Screen
						name="Loading"
						component={Loading}
						options={{ gestureEnabled: false }}
					/>
					<Stack.Screen name="Main" component={Main} />
					<Stack.Screen name="Quran" component={Quran} />
					<Stack.Screen name="QuranRead" component={QuranRead} />
					<Stack.Screen name="QuranDetail" component={QuranDetail} />
					{/* <Stack.Screen name="SignUp" component={SignUp} />
					<Stack.Screen name="SignIn" component={SignIn} />
					<Stack.Screen name="Profile" component={Profile} />
					<Stack.Screen name="Walkthrough" component={Walkthrough} /> */}
					<Stack.Screen name="Setting" component={Setting} />
					<Stack.Screen name="ThemeSetting" component={ThemeSetting} />
					{/* <Stack.Screen name="ProfileEdit" component={ProfileEdit} /> */}
					{/* <Stack.Screen name="ChangePassword" component={ChangePassword} />
					<Stack.Screen name="Home" component={Home} />
					<Stack.Screen name="Notification" component={Notification} />
					<Stack.Screen name="ShopAll" component={ShopAll} />
					<Stack.Screen name="Orders" component={Orders} />
					<Stack.Screen name="Cart" component={Cart} />
					<Stack.Screen name="News" component={News} />
					<Stack.Screen name="Whistlist" component={Whistlist} />
					<Stack.Screen name="About" component={About} />
					<Stack.Screen name="Attendance" component={Attendance} />
					<Stack.Screen name="Filter" component={Filter} />
					<Stack.Screen name="AttendanceDetail" component={AttendanceDetail} />
					<Stack.Screen name="AttendanceInOut" component={AttendanceInOut} />
					<Stack.Screen name="AttendanceCuti" component={AttendanceCuti} />
					<Stack.Screen name="AttendanceIzin" component={AttendanceIzin} />
					<Stack.Screen name="AttendanceSakit" component={AttendanceSakit} />
					<Stack.Screen name="Map" component={Map} />
					<Stack.Screen name="MapSimpok" component={MapSimpok} />
					<Stack.Screen name="SelectFlight" component={SelectFlight} />
					<Stack.Screen name="CruiseSearch" component={CruiseSearch} />
					<Stack.Screen name="SelectCruise" component={SelectCruise} />
					<Stack.Screen name="Logbook" component={Logbook} />
					<Stack.Screen name="Gita" component={Gita} />
					<Stack.Screen name="NKI" component={NKI} />
					<Stack.Screen name="Call" component={Call} />
					<Stack.Screen name="CallHistory" component={CallHistory} />
					<Stack.Screen name="CallHistoryDetail" component={CallHistoryDetail} />
					<Stack.Screen name="CallHistoryFilter" component={CallHistoryFilter} />
					<Stack.Screen name="Penilaian" component={Penilaian} />
					<Stack.Screen name="PenilaianDetail" component={PenilaianDetail} />
					<Stack.Screen name="PenilaianBuat" component={PenilaianBuat} />
					<Stack.Screen name="LogbookDetail" component={LogbookDetail} />
					<Stack.Screen name="LogbookAdd" component={LogbookAdd} />
					<Stack.Screen name="SelectDetailProject" component={SelectDetailProject} />
					<Stack.Screen name="SimpokDetail" component={SimpokDetail} />
					<Stack.Screen name="ChangeLanguage" component={ChangeLanguage} />
					<Stack.Screen name="SelectDomainOption" component={SelectDomainOption} />
					<Stack.Screen name="Camera" component={Camera} />
					<Stack.Screen name="Lembur" component={Lembur} />
					<Stack.Screen name="NKINilai" component={NKINilai} />
					<Stack.Screen name="NKIKriteria" component={NKIKriteria} />
					<Stack.Screen name="SelectUserAll" component={SelectUserAll} />
					<Stack.Screen name="ListAbsen" component={ListAbsen} /> */}
					<Stack.Screen
						name="SelectDarkOption"
						component={SelectDarkOption}
						gestureEnabled={false}
						options={{
							cardStyleInterpolator: forFade,
							cardStyle: {
								backgroundColor: 'rgba(0, 0, 0, 0.5)',
							},
						}}
					/>
					<Stack.Screen
						name="SelectFontOption"
						component={SelectFontOption}
						gestureEnabled={false}
						options={{
							cardStyleInterpolator: forFade,
							cardStyle: {
								backgroundColor: 'rgba(0, 0, 0, 0.5)',
							},
						}}
					/>
				</Stack.Navigator>
			</NavigationContainer>
		</DarkModeProvider>
	)
}
