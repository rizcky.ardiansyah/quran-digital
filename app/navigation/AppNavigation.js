import React, { useEffect } from 'react'
import { StatusBar } from 'react-native'
import { Text, TouchableOpacity, Image, View, Button } from 'react-native'
import { AnimatedTabBarNavigator } from 'react-native-animated-nav-tab-bar'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import Icon from 'react-native-vector-icons/Feather'
import styled from 'styled-components/native'
import { DarkModeProvider, useDarkMode } from 'react-native-dark-mode'
import { useTheme, BaseSetting } from '@config'
import SplashScreen from 'react-native-splash-screen'
import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import { useSelector } from 'react-redux'

const Tabs = AnimatedTabBarNavigator()

const Screen = styled.View`
	flex: 1;
	justify-content: center;
	align-items: center;
	background-color: #f2f2f2;
`

const Logo = () => (
	<Image
		source={require('./logo.png')}
		resizeMode={'cover'}
		style={{ width: 150, height: 150 }}
	/>
)

const TabBarIcon = props => {
	return (
		<Icon
			name={props.name}
			size={props.size ? props.size : 24}
			color={props.tintColor}
		/>
	)
}

const Home = props => (
	<Screen>
		<Logo />
		<Text>Home</Text>
		<TouchableOpacity onPress={() => props.navigation.navigate('Discover')}>
			<Text>Go to Discover</Text>
		</TouchableOpacity>
	</Screen>
)

const Discover = props => (
	<Screen>
		<Logo />
		<Text>Discover</Text>
		<TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
			<Text>Go to Home</Text>
		</TouchableOpacity>
	</Screen>
)

const Images = () => (
	<Screen>
		<Logo />
		<Text>Images</Text>
	</Screen>
)

const Test = () => (
	<Screen>
		<Logo />
		<Text>Test</Text>
	</Screen>
)

const Profile = () => (
	<Screen>
		<Logo />
		<Text>Profile</Text>
	</Screen>
)

function HomeScreen({ navigation }) {
	return (
		<View
			style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
			<Button
				title="Go to Profile"
				onPress={() => navigation.navigate('Profile')}
			/>
		</View>
	)
}

function ProfileScreen({ navigation }) {
	return (
		<View
			style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
			<Button
				title="Go to Notifications"
				onPress={() => navigation.navigate('Notifications')}
			/>
			<Button title="Go back" onPress={() => navigation.goBack()} />
		</View>
	)
}

function NotificationsScreen({ navigation }) {
	return (
		<View
			style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
			<Button
				title="Go to Settings"
				onPress={() => navigation.navigate('Settings')}
			/>
			<Button title="Go back" onPress={() => navigation.goBack()} />
		</View>
	)
}

function SettingsScreen({ navigation }) {
	return (
		<View
			style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
			<Button title="Go back" onPress={() => navigation.goBack()} />
		</View>
	)
}


const Stack = createStackNavigator()

function Tab() {
	return (
		<Tabs.Navigator initialRouteName="Home">
			<Tabs.Screen
				name="Home"
				component={Home}
				options={{
					tabBarIcon: ({ focused, color }) => (
						<TabBarIcon
							focused={focused}
							tintColor={color}
							name="home"
						/>
					),
				}}
			/>
			<Tabs.Screen
				name="Discover"
				component={Discover}
				options={{
					tabBarIcon: ({ focused, color, size }) => (
						<TabBarIcon
							focused={focused}
							tintColor={color}
							name="search"
						/>
					),
				}}
			/>
			<Tabs.Screen
				name="Images"
				component={Images}
				options={{
					tabBarIcon: ({ focused, color }) => (
						<TabBarIcon
							focused={focused}
							tintColor={color}
							name="image"
						/>
					),
				}}
			/>
			<Tabs.Screen
				name="Profile"
				component={Profile}
				options={{
					tabBarIcon: ({ focused, color }) => (
						<TabBarIcon
							focused={focused}
							tintColor={color}
							name="user"
						/>
					),
				}}
			/>
			<Tabs.Screen
				name="Test"
				component={Test}
				options={{
					tabBarIcon: ({ focused, color }) => (
						<TabBarIcon
							focused={focused}
							tintColor={color}
							name="user"
						/>
					),
				}}
			/>
		</Tabs.Navigator>
	)
}


const reApp = createStackNavigator({

})

export default () => {
	const storeLanguage = useSelector(state => state.application.language)
	const { theme, colors } = useTheme()
	const isDarkMode = useDarkMode()
	const forFade = ({ current, closing }) => ({
		cardStyle: {
			opacity: current.progress,
		},
	})

	useEffect(() => {
		console.log('UseEffect')
		i18n.use(initReactI18next).init({
			resources: BaseSetting.resourcesLanguage,
			lng: storeLanguage ?? BaseSetting.defaultLanguage,
			fallbackLng: BaseSetting.defaultLanguage,
		})
		SplashScreen.hide()
		StatusBar.setBackgroundColor(colors.primary, true)
		StatusBar.setBarStyle(
			isDarkMode ? 'light-content' : 'dark-content',
			true,
		)
	}, [])
	return (
		<DarkModeProvider>
			<NavigationContainer theme={theme}>
				<Stack.Navigator headerMode="none">
					<Stack.Screen name="Home" component={SettingsScreen} />
					<Stack.Screen
						name="Notifications"
						component={NotificationsScreen}
					/>
					<Stack.Screen name="Profile" component={ProfileScreen} />
					<Stack.Screen name="Settings" component={SettingsScreen} />
				</Stack.Navigator>
			</NavigationContainer>
		</DarkModeProvider>
	)
}
